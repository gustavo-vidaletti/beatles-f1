import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Login from './components/Login/Login'
import Listagem from './components/Listagem/Listagem'
import Detalhe from './components/Detalhe/Detalhe'
import HelpApi from './api/HelpApi'
import md5 from 'md5'
import UsuarioUi from './components/shared/UsuarioUi/UsuarioUi'

import './App.css';
import './constants/loader/loader.css'


class App extends Component {
  constructor(props){
    super(props)
    this.helpApi = new HelpApi();
    this.state = {
      loader: true,
      usuarioLogado: ''
    }
  }

  componentDidMount(){
    this.atualizarUsuario()
  }

  atualizarUsuario = () => {
    this.helpApi.usuarioLogado().then( () => {
      const usr = JSON.parse(localStorage.getItem(md5('usuarioLogado')))
      this.setState({
        loader: false,
        usuarioLogado: usr,
      })
    })
  }

  logout = () => {
    sessionStorage.setItem('logoutRecente', 'true')
    sessionStorage.removeItem('redirecionar')
    this.setState({
      loader: true
    })
    this.helpApi.logout().then(
      this.setState({
        loader: false,
        usuarioLogado: ''
      })
    )
  }

  refreshToken(username,password){
    this.setState({
      loader: true
    })
    this.helpApi.login(username,password).then( (response) => {
      if(!response){
        //Alertar que não foi possivel logar
        return this.logout()
      }
      this.atualizarUsuario()
    })
  }

  render() {
    const redirecionada = sessionStorage.getItem('redirecionar')
    const logoutRecente = sessionStorage.getItem('logoutRecente')
    if(this.state.loader) return (
    <div className="fundo gradiente">
      <div className="lds-dual-ring"></div>
    </div>
    )
    const { usuarioLogado } = this.state
    if( !usuarioLogado ) {
      return (
        <>
        <div className="fundo gradiente">
          <Router>
            <Switch>
              <Route path="/login" exact component={ () => <Login logou={ this.refreshToken.bind(this) }/> }/>
              <Route render={ (props) => {
                if(props.location.pathname !== "/login" && !logoutRecente ){
                  sessionStorage.setItem('redirecionar', props.location.pathname)
                }
                return (<Redirect {...props} to="/login"/>)
              } }/>
            </Switch>
          </Router>
        </div>
        </>
      )
    }
    
    sessionStorage.removeItem('redirecionar')
    sessionStorage.removeItem('logoutRecente')
    return (
      <>
      <div className="fundo gradiente">
      <Router>
        <Route path="/" render ={ (props) => <UsuarioUi {...props} logout={ this.logout.bind(this) } />}/>
        <Switch>
          {/* { historico } */}
          <Route path="/detalhe/:id" render ={ (props) => <Detalhe {...props} verificarLogin={ this.atualizarUsuario }  /> } />
          <Route exact path="/listagem" render={ (props) => <Listagem {...props} verificarLogin={ this.atualizarUsuario } /> } />
          <Route render={ () => {
            return (redirecionada && !logoutRecente) ? (<Redirect to={ redirecionada } />)  : <Redirect to="/listagem"/> 
          } }/>
        </Switch>
      </Router>
      </div>
      </>
    );
  }
}

export default App;
