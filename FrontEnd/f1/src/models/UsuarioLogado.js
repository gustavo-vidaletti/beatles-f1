export default class UsuarioLogado {
    constructor(username, tipoUsuario, email, gravatar){
        this.username = username;
        this.tipoUsuario = tipoUsuario;
        this.email = email;
        this.gravatar = gravatar
    }
}