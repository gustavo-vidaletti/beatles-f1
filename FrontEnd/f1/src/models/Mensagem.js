export default class Mensagem {
    constructor(conteudo, data, dataFormatada, username, email, gravatar){
        this.conteudo = conteudo;
        this.data = data;
        this.dataFormatada = dataFormatada
        this.username = username;
        this.email = email;
        this.gravatar = gravatar;
    }

    editarConteudo(conteudo){
        this.conteudo = conteudo;
    }
    
}