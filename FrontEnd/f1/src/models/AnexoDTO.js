export default class AnexoDTO {
    constructor(id, url, nomeDoArquivo, idChamado) {
        this.id = id
        this.url = url
        this.nomeDoArquivo = nomeDoArquivo
        this.idChamado = idChamado
    }
}