export default class ChamadoDTO {
  constructor(id, titulo, dataCriacao, dataCriacaoFormatada, dataLimite, dataLimiteFormatada, prioridade, username, status, descricao, anexos =[]) {
    this.id = id
    this.titulo = titulo
    this.dataCriacao = dataCriacao
    this.dataCriacaoFormatada = dataCriacaoFormatada
    this.dataLimite = dataLimite
    this.dataLimiteFormatada = dataLimiteFormatada
    this.prioridade = prioridade
    this.username = username
    this.status = status
    this.descricao = descricao
    this.anexos = anexos
  }
}