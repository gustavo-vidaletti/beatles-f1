export default class Chamado {
    constructor(id = 0, titulo, status, descricao, prioridade, dataCriacao = null, dataCriacaoFormatada, dataLimite, dataLimiteFormatada, anexos, usuario){
        this.id = id
        this.status = status
        this.titulo = titulo
        this.prioridade = prioridade
        this.descricao = descricao
        this.dataCriacao = dataCriacao
        this.dataCriacaoFormatada = dataCriacaoFormatada
        this.dataLimite = dataLimite
        this.dataLimiteFormatada = dataLimiteFormatada
        this.anexos = anexos
        this.usuario = usuario
    }
}