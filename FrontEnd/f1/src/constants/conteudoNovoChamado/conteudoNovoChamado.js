import React, { Component } from 'react'
import * as moment from 'moment';
import {
  Form, FormGroup, Label, Input, FormText, CustomInput, UncontrolledCollapse, Button, CardBody, Card
} from 'reactstrap';
import InputFile from '../../components/shared/inputFile/InputFile'
import ChamadoDTO from '../../models/ChamadoDTO';

moment.locale = 'pt-br'
export default class ConteudoNovoChamado extends Component {
  
  constructor(props) {
    super(props)
    this.arquivo1 = React.createRef();
    this.arquivo2 = React.createRef();
    this.arquivo3 = React.createRef();

    this.state = {
      deveEditar: false,
      id: '',
      titulo: '',
      descricao: '',
      dataCriacao: '',
      dataLimite: '',
      prioridade: 'NORMAL',
      anexos: []
    }
  }

  componentDidMount() {
    const { chamado } = this.props
    this.atualizarEstado(chamado)
  }

  atualizarEstado = (chamado) => {
    if (chamado) {
      this.setState({
        id: chamado.id,
        titulo: chamado.titulo,
        descricao: chamado.descricao,
        dataCriacao: chamado.dataCriacao,
        dataLimite: moment(chamado.dataLimite).format('YYYY-MM-DD'),
        prioridade: chamado.prioridade,
        anexos: chamado.anexos,
        status: chamado.status
      })
    }
  }

  adicionarAnexo = (file) => {
    const anex = this.state.anexos
    anex.push(file)
    this.setState({
      anexos: anex
    })
  }

  getChamado = () => {
    const { id, titulo, descricao, dataCriacao, dataLimite, prioridade, anexos, status, username } = this.state
    const chamado = new ChamadoDTO(
      id, titulo,
      moment(dataCriacao).add(3,'h').format('YYYY-MM-DDTHH:mm:ss'), 
      moment(dataCriacao).format('DD/MM/YYYY - HH:mm'), 
      moment(dataLimite).add(3,'h').format('YYYY-MM-DDTHH:mm:ss'), 
      moment(dataLimite).format('DD/MM/YYYY - HH:mm'),
      prioridade,
      username,
      status,
      descricao,
      anexos)

    this.atualizarEstado(chamado)
    return chamado
  }

  render() {
    return (
      <>
        <Form>
          <FormGroup>
            <Label for="titulo">Título</Label>
            <Input type="text" name="titulo" id="titulo" value={this.state.titulo} onChange={(evt) => { this.setState({ titulo: evt.target.value }) }} required />
          </FormGroup>
          <FormGroup>
            <Label for="exampleText">Descrição</Label>
            <Input className="form-control" type="textarea" name="text" id="textDescricao" value={this.state.descricao} onChange={(evt) => { this.setState({ descricao: evt.target.value }) }} required />
          </FormGroup>

          <FormGroup>
            <div className="limite">
              <Label for="exampleDate">Data de limite</Label>
              <Input className="form-control" type="date" name="date" id="data" placeholder="date placeholder" value={this.state.dataLimite} onChange={(evt) => { this.setState({ dataLimite: evt.target.value }) }} />
            </div>
          </FormGroup>
          {( (this.state.id && this.state.anexos.length < 3) || (!this.state.id) ) ?
            (<>
              <div className="w-100 mb-2">
                <Button color="secondary" id="toggler" className="w-100" style={{ marginBottom: '1rem' }}>Anexos</Button>
                <UncontrolledCollapse toggler="#toggler">
                  <Card>
                    <CardBody>
                      <FormGroup>
                        <Label for="exampleFile">Anexos</Label>
                        <div>
                          { ((this.state.id && this.state.anexos && this.state.anexos.length < 3) || (!this.state.id)) ?
                            (<div className="mb-3">
                              <InputFile adicionarAnexo={this.adicionarAnexo} ref={this.arquivo1} />
                            </div>) : ''}
                          { ((this.state.id && this.state.anexos && this.state.anexos.length < 2) || (!this.state.id)) ?
                            (<div className="mb-3">
                              <InputFile adicionarAnexo={this.adicionarAnexo} ref={this.arquivo2} />
                            </div>) : ''
                          }
                          { ((this.state.id && this.state.anexos && this.state.anexos.length < 1) || (!this.state.id)) ?
                            (<div className="mb-3">
                              <InputFile adicionarAnexo={this.adicionarAnexo} ref={this.arquivo3} />
                            </div>) : ''
                          }
                        </div>
                        {(!this.state.id) ? (<FormText color="muted">Não é necessário anexar um arquivo para criar um novo chamado!</FormText>) : ''}
                      </FormGroup>
                    </CardBody>
                  </Card>
                </UncontrolledCollapse>
              </div>
            </>)
            : ''}
          <FormGroup tag="fieldset">
            <Label for="exampleFile">Prioridade</Label>
            <FormGroup>
              <CustomInput type="select" id="exampleCustomSelect" value={this.state.prioridade} onChange={(e) => { this.setState({ prioridade: e.target.value }) }} name="customSelect">
                <option value="ALTA">Alta</option>
                <option value="NORMAL">Normal</option>
                <option value="BAIXA">Baixa</option>
              </CustomInput>
            </FormGroup>
          </FormGroup>
        </Form>
      </>
    )
  }
}