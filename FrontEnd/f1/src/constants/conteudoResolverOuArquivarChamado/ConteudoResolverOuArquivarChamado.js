import React, { Component } from 'react';
import {FormGroup, Label, Input } from 'reactstrap'

export default class ConteudoResolverOuArquivarChamado extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mensagem: ''
    }
  }

  getMensagem(){
    return this.state.mensagem
  }

  render() {
    return (
      <>
      <FormGroup>
          <Label for="exampleText">Deseja mesmo { this.props.acao } este chamado?</Label>
          <Input type="textarea" name="text" id="textarea" placeholder="Mensagem final" 
            value={this.state.mensagem}
            onChange={ (e) => this.setState({ mensagem: e.target.value }) }
            />
        </FormGroup>
      </>
    )
  }
}