import React, { Component } from 'react';
import { Button, Form, FormGroup, Input, Container, Row, Col, Label, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import HelpApi from '../../api/HelpApi'
import './Login.css'

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.api = new HelpApi()
    this.refSenha = React.createRef()
    this.state = {
      username: '',
      password: '',
      modal: false,
      nomeCadastro: '',
      emailCadastro: '',
      emailCadastroValido: true,
      usernameCadastro: '',
      usernameCadastroValido: true,
      senhaCadastro: '',
      confirmaSenhaCadastro: '',
      confereSenha: true,
      fadeIn: false

    }
  }

  componentDidMount(){
    this.setState({
      fadeIn: true
    })
  }

  testLogin(e) {
    e.preventDefault();
    const { username, password } = this.state
    this.props.logou(username, password)
  }

  toggle = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  toggleEnviar = () => {
    const usuario = {
      nome: this.state.nomeCadastro,
      email: this.state.emailCadastro,
      username: this.state.usernameCadastro,
      password: this.state.senhaCadastro,
      tipoUsuario: 'COMUM'

    }

    this.api.registrarUsuario(usuario).then(
      this.setState({
        username: usuario.username,
        password: usuario.password
      })
    )
    this.toggle()
  }

  testaEmail = () => {
    if(this.state.emailCadastro)
    this.api.contemEmail(this.state.emailCadastro).then( (resp) => {
      this.setState({
        emailCadastroValido: !resp
      })
    })
  }

  testaUsuario = () => {
    if(this.state.usernameCadastro)
    this.api.contemUsuario(this.state.usernameCadastro).then( (resp) => {
      const valor = (resp.data === true)? true : false
      this.setState({
        usernameCadastroValido: !valor
      })
    })
  }

  confereAsSenhas = () => {
    const confere = 
    this.state.senhaCadastro === this.state.confirmaSenhaCadastro
    this.setState({
      confereSenha: confere
    })
  }


  render() {
    // const tk = window.localStorage.getItem('')
    // if(!tk) return (<Redirect to="/"/>)

    return (
      <>
          <Container className={`d-flex justify-content-center ${(this.state.fadeIn)?'aparecer' : 'esconder'}`}>
            <Form className="rounded p-4 login" onSubmit={this.testLogin.bind(this)}>
              <Row style={{ 'marginLeft': 0, 'marginRight': 0 }}>
                <Col className="text-center rounded">
                  <h1 className="titulo">F1 - Help! </h1>
                  <label className="subtitulo">Sistema de Chamados </label>
                </Col>
              </Row>
              <Row className="d-flex justify-content-center mt-3" style={{ 'marginLeft': 0, 'marginRight': 0 }}  >
                <FormGroup>
                  <Col>
                    <Input className="input-login mb-3" type="text" name="username" placeholder="Usuário" value={this.state.username} onChange={(e) => this.setState({ username: e.target.value })} />
                    <Input className="input-login mb-2" type="password" name="password" value={this.state.password} onChange={(e) => this.setState({ password: e.target.value })} placeholder="Senha" />
                  </Col>
                </FormGroup>
              </Row>
              <Row style={{ 'marginLeft': 0, 'marginRight': 0 }}>
                <Col className="d-flex justify-content-center">
                  <Button className="botao-login mb-4" disabled={(this.state.password.length === 0 || this.state.username.length === 0) ? true : false}>Entrar</Button>
                </Col>
              </Row>
              <Row>
                <Col>
                  <label onClick={this.toggle}  className="link-login" >Cadastre-se aqui</label>
                </Col>
              </Row>
            </Form>
          </Container>
          {/* <Button id="bntNovoUsuario">{this.props.buttonLabel}Criar usuário!</Button> */}
        <Modal centered={true} isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Novo Usuário:</ModalHeader>
          <ModalBody>
            
          <Form>
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="nome">Nome completo</Label>
              <Input type="nome" name="nome" id="nome" placeholder="nome" value={ this.state.nomeCadastro } onChange={ (evt) => { this.setState({ nomeCadastro: evt.target.value })} } required/>
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="username">Username</Label>
              <Input type="text" name="username" id="username" className={ (this.state.usernameCadastroValido ) ? '': 'alarme'} placeholder="username" value={ this.state.usernameCadastro } onChange={ (evt) => { this.setState({ usernameCadastro: evt.target.value, usernameCadastroValido: true })} } onBlur={ this.testaUsuario } required/>
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input type="email" name="email" id="email" className={ (this.state.emailCadastroValido)? '': 'alarme'} placeholder="email" value={ this.state.emailCadastro } onChange={ (evt) => { this.setState({ emailCadastro: evt.target.value, emailCadastroValido: true })} } onBlur={ this.testaEmail } required/>
        </FormGroup>
        <FormGroup>
          <Label for="senha-modal">Password</Label>
          <Input type="password" name="senha-modal" id="senha-modal" placeholder="Senha" value={ this.state.senhaCadastro } onChange={ (evt) => { this.setState({ senhaCadastro: evt.target.value })}  }  onBlur={this.confereAsSenhas} required/>
        </FormGroup>
        <FormGroup>
          <Label for="confirma-Senha">Confirme sua senha</Label>
          <Input type="password" name="confirma-Senha" id="confirma-Senha" className={ (this.state.confereSenha)? '': 'alarme'} placeholder="Confirmar Senha" value={ this.state.confirmaSenhaCadastro } onChange={ (evt) => { this.setState({ confirmaSenhaCadastro: evt.target.value })} } onBlur={this.confereAsSenhas} required/>
        </FormGroup>
        </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggle}>Cancelar</Button>
            <Button color="primary" disabled={
              (this.state.confereSenha && 
                this.state.emailCadastroValido && this.state.usernameCadastroValido
                ) ? false : true
            } onClick={ this.toggleEnviar }>Cadastrar</Button>{' '}
          </ModalFooter>
        </Modal>
      </>
    )
  }
}