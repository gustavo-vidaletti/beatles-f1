import React, { Component } from 'react';
import { Dropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';
import { Link } from 'react-router-dom'
import md5 from 'md5';
import './UsuarioUi.css';
import HelpApi from '../../../api/HelpApi';
import PropTypes from 'prop-types'

export default class UsuarioUi extends Component {

  constructor(props) {
    super(props)
    this.api = new HelpApi()
    this.state = {
      deveAparecer: false,
      usuarioLogado: '',
      isOpen: false
    }
  }

  componentDidMount() {
    const us = JSON.parse(localStorage.getItem(md5('usuarioLogado')))
    setTimeout(() => {
      this.setState({
        usuarioLogado: us,
        deveAparecer: true
      })
    }, 1100)
  }

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }


  render() {
    const { isOpen } = this.state
    return this.state.deveAparecer && (
      <>
        <Dropdown className="" direction="up" isOpen={isOpen} toggle={this.toggle}>
          <DropdownToggle className="redondo" style={{ 'backgroundImage': `url("${this.state.usuarioLogado.gravatar}")` }}>
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem header>{this.state.usuarioLogado.username}</DropdownItem>
            <Link className="link" to="/listagem">
              <DropdownItem className="item-drop">
              Listagem
              </DropdownItem>
            </Link>
            <DropdownItem divider />
            <DropdownItem className="item-drop" onClick={this.props.logout}>Logout</DropdownItem>
          </DropdownMenu>
        </Dropdown>
      </>
    )
  }
}

UsuarioUi.propTypes = {
  usuarioLogado: PropTypes.string,
  deveAparecer: PropTypes.bool,
  isOpen: PropTypes.bool
}