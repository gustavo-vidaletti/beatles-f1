import React, { Component } from 'react';
import { Button } from 'reactstrap';
import HelpApi from '../../../api/HelpApi';
import { FaDownload } from "react-icons/fa";
import PropTypes from 'prop-types'

export default class DownloadFile extends Component {
    constructor(props) {
        super(props)
        this.api = new HelpApi()
        this.state = {
            anexo: this.props.anexo
        }
    }
    
    download(){
        const {nomeDoArquivo, id} = this.state.anexo
        this.api.downloadAnexo(id, nomeDoArquivo)
    }

    render() {
        return (
            <>
                <Button className="mr-2 ml-2" onClick={ this.download.bind(this) } color="secondary" ><FaDownload className = "mr-1" />{ this.state.anexo.nomeDoArquivo}</Button>
            </>
        )
    }
}

DownloadFile.propTypes = {
    anexo: PropTypes.object
}
