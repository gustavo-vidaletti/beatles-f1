import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Tooltip } from 'reactstrap';
import PropTypes from 'prop-types'

export default class ModalComponente extends Component {

  constructor(props, funcao, mensagemBotao, conteudoModal) {
    super(props);
    this.funcao = funcao
    this.mensagemBotao = mensagemBotao
    this.conteudoModal = conteudoModal
    this.state = {
      isOpen: false,
      tooltipOpen: false
    };

  }

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  toggleTooltip = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    })
  }

  chamaEFecha = () => {
    this.props.funcao()
    this.toggle()
  }

  render() {
    return (
      <>
        <div>
          <Button id={ `${this.props.mensagemTooltip}-${this.props.idChamado}` } color={this.props.colorBotao} onClick={this.toggle} className={this.props.classBotao}>{this.props.mensagemBotao}</Button>
          <Tooltip placement="right" isOpen={this.state.tooltipOpen} target={ `${this.props.mensagemTooltip}-${this.props.idChamado}` } toggle={ this.toggleTooltip }>
            { this.props.mensagemTooltip }
          </Tooltip>
          <Modal centered={true} isOpen={this.state.isOpen} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{(this.props.mensagem) ? this.props.mensagem : `${this.props.mensagemBotao} chamado?`}</ModalHeader>

            <ModalBody>
              {this.props.conteudoModal}
            </ModalBody>

            <ModalFooter>
              <Button color="light" onClick={this.toggle}>Cancelar</Button>
              <Button color={this.props.color} onClick={this.chamaEFecha}>{(this.props.mensagemBotaoSalvar) ? this.props.mensagemBotaoSalvar : this.props.mensagemBotao}</Button>{' '}
            </ModalFooter>
          </Modal>
        </div>
      </>
    )
  }
}

ModalComponente.propTypes = {
  funcao: PropTypes.func,
  isOpen: PropTypes.bool
}