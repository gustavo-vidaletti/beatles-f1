import React, { Component } from 'react';
import HelpApi from '../../../api/HelpApi';

export default class InputFile extends Component {

  constructor(props) {
    super(props)
    this.api = new HelpApi();
    this.state = {
      file: '',
    }
  }

  onFileChange = (event) => {
    this.setState({
      file: event.target.files[0]
    })
    this.props.adicionarAnexo(event.target.files[0])
  }

  uploadFile = (event) => {
    event.preventDefault();
    this.setState({
      error: '',
      msg: ''
    })

    if (!this.state.file) {
      //Nada colocado
      return;
    }


    if (this.state.file.size >= 2000000) {
      //arquivo muito grande
      return;
    }

    let data = new FormData();
    data.append('file', this.state.file);


    this.api.novoAnexo(data, this.props.idChamado).then((resp) => {
      if (resp) {
        //sucesso
      }
    }).catch(err => {
      // erro no upload
      return;
    });
  }

  render() {
    return (
      <>
        <div className="input-group">
          <div className="custom-file">
            <input type="file" className="custom-file-input" id="input" onChange={ this.onFileChange } />
            <label className="custom-file-label" htmlFor="input">{ (this.state.file) ? this.state.file.name : "Selecionar o arquivo..."}</label>
          </div>
        </div>
      </>
    )
  }
}