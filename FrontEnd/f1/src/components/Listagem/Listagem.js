import React, { Component } from 'react';
import HelpApi from '../../api/HelpApi';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col, Table, Navbar, Nav, NavbarToggler, Collapse, Input } from 'reactstrap';
import { FaPlus, FaArchive, FaCheckSquare } from 'react-icons/fa'
import md5 from 'md5'
import './Listagem.css'
import '../../utils/array-prototype'
import ModalComponente from '../shared/modal/ModalComponente';
import ConteudoNovoChamado from '../../constants/conteudoNovoChamado/conteudoNovoChamado';
import ConteudoResolverOuArquivarChamado from '../../constants/conteudoResolverOuArquivarChamado/ConteudoResolverOuArquivarChamado'
import ChamadoDTO from '../../models/ChamadoDTO';
export default class Listagem extends Component {

  constructor(props) {
    super(props)
    this.api = new HelpApi()
    this.conteudoRef = React.createRef();
    this.arquivarRef = React.createRef();
    this.resolverRef = React.createRef();
    this.state = {
      isOpen: false,
      redirecionar: false,
      usuarioLogado: '',
      loader: true,
      filtroDeUsuario: 'Todos usuarios',
      filtroDeStatus: 'ABERTO',
      usuarios: [],
      chamados: [],
      chamadosFiltrados: [],
      fadeIn: false
    }
  }

  toggleCollapseTabela = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  componentDidMount() {
    this.props.verificarLogin()
    const us = JSON.parse(localStorage.getItem(md5('usuarioLogado')))
    this.api.listarChamados().then((response) => {

      this.api.listarUsernames().then((username) => {
        const chamados = response.ordenarChamados()
        this.setState({
          usuarioLogado: us,
          loader: false,
          chamados: chamados,
          chamadosFiltrados: chamados,
          usuarios: username,
          fadeIn: true
        })
        this.filtrar()
      }
      )
    })
  }

  componentWillUnmount(){
    this.setState({
      fadeIn: false      
    })
  }

  mostrarColunaCriador = (chamado) => {
    return (this.state.usuarioLogado.tipoUsuario === 'OPERADOR') ? (<td onClick={this.ativarRedirecionamento.bind(this, chamado.id)}>{chamado.username}</td>) : null
  }

  mostrarBotoesDeOperacao = (chamado) => {
    const { usuarioLogado } = this.state
    let resolver = usuarioLogado.tipoUsuario === 'OPERADOR'
    let arquivar = usuarioLogado.username === chamado.username
    resolver = (resolver && chamado.status === 'ABERTO') ?
    <>
      <ModalComponente
        mensagemBotao={(<FaCheckSquare />)}
        classBotao="botao-tabela plus opcoes btn-success"
        mensagemTooltip="Resolver"
        idChamado={chamado.id}
        mensagem="Resolver chamado"
        color="success"
        mensagemBotaoSalvar="Resolver"
        funcao={ this.resolver.bind(this,chamado.id) }
        conteudoModal={ <ConteudoResolverOuArquivarChamado ref={this.resolverRef} acao="Resolver"/>}
      />
    </>
      
      : ''


    arquivar = (arquivar && chamado.status === 'ABERTO' ) ?
      <ModalComponente
        mensagemBotao={(<FaArchive />)}
        classBotao="botao-tabela plus opcoes btn-danger"
        mensagemTooltip="Arquivar"
        idChamado={chamado.id}
        mensagem="Arquivar chamado"
        color="danger"
        mensagemBotaoSalvar="Arquivar"
        funcao={this.arquivar.bind(this, chamado.id)}
        conteudoModal={ <ConteudoResolverOuArquivarChamado ref={this.arquivarRef} acao="Arquivar"/>}
      />
      : ''


    return (<td><div className="operacoes">{arquivar}{resolver}</div></td>)
  }

  arquivar = (idChamado) => {
    const msg = this.arquivarRef.current.getMensagem()
    this.api.arquivarChamado(idChamado, msg).then( (response) => {
      this.componentDidMount()
    })
  }
  
  resolver = (idChamado) => {
    const msg = this.resolverRef.current.getMensagem()
    this.api.resolverChamado(idChamado, msg).then( (response) => {
      this.componentDidMount()
    })
  }


  usernamesNoInput = () => {
    return (this.state.usuarios.sort().map((nome) => {
      return (<option key={nome} value={nome} >{nome}</option>)
    }))
  }

  ativarRedirecionamento = (idChamado) => {
    this.setState({
      redirecionar: idChamado
    })
  }

  mudouFiltroStatus = (evt) => {
    this.filtrar(null, evt.target.value)
  }

  mudouFiltroUsuario = (evt) => {
    this.filtrar(evt.target.value)
  }

  filtrar = (novoFiltroUsuario = null, novoFiltroStatus = null) => {
    const { chamados } = this.state
    let { filtroDeUsuario, filtroDeStatus } = this.state
    let filtrado = chamados
    if (novoFiltroStatus) {
      filtroDeStatus = novoFiltroStatus
    }

    if (novoFiltroUsuario) {
      filtroDeUsuario = novoFiltroUsuario
    }

    if (filtroDeUsuario !== 'Todos usuarios') {
      filtrado = filtrado.filtrarPorUsername(filtroDeUsuario)
    }

    filtrado = filtrado.filtrarPorStatus(filtroDeStatus)

    this.setState({
      chamadosFiltrados: filtrado,
      filtroDeUsuario,
      filtroDeStatus
    })

  }

  enviarChamadoEArquivos = () => {
    const objeto = this.conteudoRef.current.getChamado();
    const chamado = new ChamadoDTO(objeto.id, objeto.titulo,
      null, null,
      objeto.dataLimite, objeto.dataLimiteFormatada,
      objeto.prioridade, objeto.username,
      objeto.status, objeto.descricao,
      objeto.anexos)

    //Não dá pra usar promiseALL nessa primeira porque precisa do id
    this.api.novoChamado(chamado).then((chamadoRetornado) => {
      if(!chamadoRetornado) return;
      const promessas = []
      const id = chamadoRetornado.id
      objeto.anexos.forEach((anexo) => {
        promessas.push(this.api.novoAnexoByFile(anexo, id))
      })

      Promise.all(promessas).then((respostas) => {
        this.componentDidMount()
      })

    })
  }

  render() {
    const { loader, redirecionar, usuarioLogado } = this.state;
    if (loader) {
      return (<div className="lds-dual-ring"></div>)
    }

    if (redirecionar) {
      return (<Redirect to={`/detalhe/${redirecionar}`} />)
    }

    return (
      <>
        <Container>
        {/* <Fade in={this.state.fadeIn} unmountOnExit mountOnEnter> */}
          <div className={`bloco sombra ${(this.state.fadeIn)?'aparecer' : 'esconder'}`}>
            <Row>
              <Col>
                <div>
                  <Navbar className="navbar-tabela top" color="light" light expand="md">
                    <h4><b>Listagem de Chamados</b></h4>
                    <NavbarToggler onClick={this.toggleCollapseTabela} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                      <Nav className="ml-auto" navbar>
                        <Input className="input-tabela" type="select" name="select" id="exampleSelect" value={this.state.filtroDeStatus} onChange={this.mudouFiltroStatus}>
                          <option>ABERTO</option>
                          <option>ARQUIVADO</option>
                          <option>RESOLVIDO</option>

                        </Input>
                        {(this.state.usuarioLogado.tipoUsuario === 'OPERADOR') ? (
                          <Input className="input-tabela" type="select" name="select" id="exampleSelect" value={this.state.filtroDeUsuario} onChange={this.mudouFiltroUsuario}>
                            <option>Todos usuarios</option>
                            {this.usernamesNoInput()}
                          </Input>) : ''}

                        <ModalComponente
                          mensagemBotao={ (<FaPlus />) }
                          color='success'
                          classBotao="plus b-plus"
                          mensagemTooltip="Adicionar"
                          idChamado="adicionar"
                          mensagem="Adicionar um Chamado"
                          mensagemBotaoSalvar="Adicionar"
                          funcao={this.enviarChamadoEArquivos}
                          conteudoModal={<ConteudoNovoChamado ref={this.conteudoRef} />}
                        />
                      </Nav>
                    </Collapse>
                  </Navbar>
                </div>
              </Col>
            </Row>

            <Row>
              <Col>
                <Table className="tabela table-fixed" hover responsive={true} striped bordered>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Titulo</th>
                      <th>Criação</th>
                      <th>Limite</th>
                      <th>Prioridade</th>
                      {(usuarioLogado.tipoUsuario === 'OPERADOR') ? (<th>Criador</th>) : null}
                      <th>Operações</th>
                    </tr>
                  </thead>
                  <tbody>
                    {(this.state.chamadosFiltrados) ? (
                      this.state.chamadosFiltrados.map((chamado) => {
                        return (
                          <tr key={chamado.id} style={{ 'cursor': 'pointer' }}>
                            <th scope="row" onClick={this.ativarRedirecionamento.bind(this, chamado.id)}>{chamado.id}</th>
                            <td onClick={this.ativarRedirecionamento.bind(this, chamado.id)} >{chamado.titulo}</td>
                            <td onClick={this.ativarRedirecionamento.bind(this, chamado.id)} >{chamado.dataCriacaoFormatada}</td>
                            <td onClick={this.ativarRedirecionamento.bind(this, chamado.id)} >{chamado.dataLimiteFormatada}</td>
                            <td onClick={this.ativarRedirecionamento.bind(this, chamado.id)} >{chamado.prioridade}</td>
                            {this.mostrarColunaCriador(chamado)}
                            {this.mostrarBotoesDeOperacao(chamado)}
                          </tr>
                        )
                      })
                    ) : ''}
                  </tbody>
                </Table>
                <Navbar className="navbar-tabela bottom" color="light" light expand="md">
                  {/* <h7><b>Listagem de Chamados</b></h7> */}
                </Navbar>
              </Col>
            </Row>
          </div>
        {/* </Fade> */}
        </Container>
      </>
    )
  }
}