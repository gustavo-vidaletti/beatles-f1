import React, { Component, Fragment } from 'react'

export default class BotaoForm extends Component{
    
    constructor(props, mensagemBotao, funcao) {
        super(props)   
        this.mensagemBotao = mensagemBotao 
        this.funcao = funcao
    }

    render() {
        return(
            <Fragment>
                <div className="ui blue labeled submit icon button" onClick={ this.props.funcao }>
                        <i className="icon edit"></i> { this.props.mensagemBotao }
                </div>
            </Fragment>
        )
    }
}