import React, { Component, Fragment } from 'react'
import './Mensagem.css'

export default class MensagemForm extends Component{
    constructor(props, mensagemBotao, funcao) {
        super(props)
        this.mensagemBotao = mensagemBotao
        this.funcao = funcao
        this.state = {
          mensagem: '',
          mensagens: []
        }
    }

      render() {
        return (
          <Fragment>
            <div className="comment">
                <form className="ui reply form">
                    <div className="field">
                        <textarea rows="2" 
                          name="mensagem"
                          onChange={e => this.setState({mensagem: e.target.value})}
                          value={this.state.mensagem}
                          placeholder="Digite sua mensagem"
                        />
                    </div>
                </form> 
            </div>
          </Fragment>
        )
      }    
}