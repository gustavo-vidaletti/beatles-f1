import React, { Component } from 'react';
import { Container, Row, Col, Button, Input } from 'reactstrap';
import { Route, Redirect } from 'react-router-dom';
import { FaArchive, FaCheckSquare, FaPencilAlt, FaAngleLeft } from 'react-icons/fa'
import HelpApi from '../../api/HelpApi';
import DownloadFile from '../shared/downloadFile/DownloadFile';
import ModalComponente from '../shared/modal/ModalComponente';
import MensagemUi from './MensagemUi';
import ConteudoNovoChamado from '../../constants/conteudoNovoChamado/conteudoNovoChamado'
import ConteudoResolverOuArquivarChamado from '../../constants/conteudoResolverOuArquivarChamado/ConteudoResolverOuArquivarChamado'
import ChamadoDTO from '../../models/ChamadoDTO'
import * as moment from 'moment'
import md5 from 'md5'
import '../../utils/array-prototype'
import './Detalhe.css'

moment.locale = 'pt-br'
export default class Detalhe extends Component {

  constructor(props) {
    super(props)
    this.api = new HelpApi()
    this.arquivarRef = React.createRef();
    this.resolverRef = React.createRef();
    this.conteudoRef = React.createRef();
    this.state = {
      loader: true,
      chamado: '',
      usuarioLogado: '',
      listaMensagens: [],
      conteudoNovaMensagem: '',
      conteudoMensagemFinal: '',
      modalArquivar: false,
      modalResolver: false,
      redirecionar: false,
      fadeIn: false
    }
  }

  componentDidMount() {
    this.props.verificarLogin()
    const us = JSON.parse(localStorage.getItem(md5('usuarioLogado')))
    const { id } = this.props.match.params
    const requests = [
      this.api.getDetalhesChamado(id),
      this.api.getMensagensDoChamado(id)
    ]
    Promise.all(requests).then((responses) => {
      if (!responses[0]) {
        this.setState({
          redirecionar: true
        })
      }
      this.setState({
        loader: false,
        chamado: responses[0],
        listaMensagens: responses[1].ordenarMensagens(),
        usuarioLogado: us,
        fadeIn: true
      })
      this.mostrarModais()
    })

  }

  atualizarConteudoMensagemEnviarMensagem = () => {
    const mensagemParaAdicionar = this.state.conteudoNovaMensagem

    this.api.responderChamado(this.state.chamado.id, mensagemParaAdicionar).then((response) => {
      listaMensagens.push(response)
      this.setState({
        listaMensagens,
        conteudoNovaMensagem: ''
      })
    })

    const { listaMensagens } = this.state
  }

  arquivar = (idChamado) => {
    const msg = this.arquivarRef.current.getMensagem()
    this.api.arquivarChamado(idChamado, msg).then((response) => {
      this.componentDidMount()
    })
  }

  resolver = (idChamado) => {
    const msg = this.resolverRef.current.getMensagem()
    this.api.resolverChamado(idChamado, msg).then((response) => {
      this.componentDidMount()
    })
  }

  atualizarMensagensDoSistema = () => {
    this.api.getMensagensDoChamado(this.state.chamado.id).then((response) => {
      this.setState({
        listaMensagens: response
      })
    })
  }

  mostrarModais() {
    let arquivar = false
    let resolver = false
    if (this.state.usuarioLogado.tipoUsuario === "OPERADOR" && this.state.chamado.status === 'ABERTO') { resolver = true }
    if (this.state.usuarioLogado.username === this.state.chamado.username && this.state.chamado.status === 'ABERTO') { arquivar = true }
    this.setState({
      modalArquivar: arquivar,
      modalResolver: resolver,
      modalEditar: arquivar
    })
  }

  atualizarChamado = () => {
    const chamado = this.conteudoRef.current.getChamado()
    this.api.novoChamado(chamado).then((response) => {
      const { id, titulo, dataCriacao, dataLimite, prioridade, status, descricao, anexos } = response
      const chamado = new ChamadoDTO(
        id,
        titulo,
        moment(dataCriacao).add(3, 'h').format('YYYY-MM-DDTHH:mm:ss'),
        moment(dataCriacao).format('DD/MM/YYYY - HH:mm'),
        moment(dataLimite).add(3, 'h').format('YYYY-MM-DDTHH:mm:ss'),
        moment(dataLimite).format('DD/MM/YYYY - HH:mm'), prioridade,
        this.state.chamado.username, status, descricao, anexos
      )

      this.setState({
        chamado: chamado
      })
    })
  }

  render() {
    if (this.state.redirecionar) {
      return (
        <>
          <Route render={() => <Redirect to="/listagem" />} />
        </>
      )
    }
    const DownloadAnexo = (this.state.chamado) ? this.state.chamado.anexos.map((a) => <DownloadFile anexo={a} key={a.id} />) : ""
    return (
      <>

        <div className={`root-detalhe sombra mt-5 mb-5 rounded ${(this.state.fadeIn)?'aparecer' : 'esconder'}`}>
          <Container>
            <Row className="d-flex justify-content-space-between">
              <Col sm="6" className="mt-3">
                <Button color="link" className="voltar" onClick={() => { this.setState({ redirecionar: true }) }}><FaAngleLeft />Voltar</Button>
              </Col>
              <Col sm='6' className="mt-3">
                <div className="d-flex justify-content-end botoes">
                  {(this.state.modalResolver) ? (
                    <div>
                      <ModalComponente
                        mensagemBotao={(<FaCheckSquare />)}
                        classBotao="botao-tabela plus opcoes btn-success"
                        mensagemTooltip="Resolver"
                        idChamado={this.state.chamado.id}
                        mensagem="Resolver chamado"
                        color="success"
                        mensagemBotaoSalvar="Resolver"
                        funcao={this.resolver.bind(this, this.state.chamado.id)}
                        conteudoModal={<ConteudoResolverOuArquivarChamado ref={this.resolverRef} acao="Resolver" />}
                      />
                    </div>
                  ) : ''}
                  {/* modal arquivar chamado */}
                  {(this.state.modalArquivar) ? (
                    <div>
                      <ModalComponente
                        mensagemBotao={(<FaArchive />)}
                        classBotao="botao-tabela plus opcoes btn-danger"
                        mensagemTooltip="Arquivar"
                        idChamado={this.state.chamado.id}
                        mensagem="Arquivar chamado"
                        color="danger"
                        mensagemBotaoSalvar="Arquivar"
                        funcao={this.arquivar.bind(this, this.state.chamado.id)}
                        conteudoModal={<ConteudoResolverOuArquivarChamado ref={this.arquivarRef} acao="Arquivar" />}
                      />
                    </div>
                  ) : ''}
                  {
                    (this.state.modalEditar) ?
                      <ModalComponente
                        mensagemBotao={(<FaPencilAlt />)}
                        color='info'
                        classBotao="plus b-plus lapis"
                        mensagemTooltip="Editar"
                        idChamado={this.state.chamado.id}
                        mensagem="Editar um Chamado"
                        mensagemBotaoSalvar="Editar"
                        funcao={this.atualizarChamado}
                        conteudoModal={<ConteudoNovoChamado chamado={this.state.chamado} ref={this.conteudoRef} />}
                      />
                      : ''
                  }
                </div>
              </Col>
            </Row>

            <Row className="mb-4 mt-3">
              <Col sm="12">
                <h3>{this.state.chamado.titulo} - {this.state.chamado.id}</h3>
              </Col>
            </Row>

            <Row className="mb-3">
              <Col sm="4">
                <div className="custom-infos text-center rounded h-100 ">
                  <h5>Data de Criação</h5>
                  <p>{this.state.chamado.dataCriacaoFormatada}</p>
                </div>
              </Col>
              <Col sm="4">
                <div className="custom-infos text-center rounded h-100 ">
                  <h5>Data Limite</h5>
                  <p>{this.state.chamado.dataLimiteFormatada}</p>
                </div>
              </Col>
              <Col sm="4">
                <div className="custom-infos text-center rounded h-100 ">
                  <h5>Prioridade</h5>
                  <p>{this.state.chamado.prioridade}</p>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <div className="custom-infos text-center rounded h-100 ">
                  <h5>Descrição</h5>
                  <p>{this.state.chamado.descricao}</p>
                </div>
              </Col>
            </Row>

            <Row className="mr-2 ml-2">
              <Col sm="12" className="custom-download text-center p-3">
                {DownloadAnexo}
              </Col>
            </Row>

            <Row className="mb-3">
              <div className="ui container comments">
                {<MensagemUi listaMensagens={this.state.listaMensagens} />}
              </div>
            </Row>

            <Row>
              <Col sm="12">
                <div className="comment">
                  <form className="ui reply form">
                    <div className="field">
                      <Input
                        disabled={(this.state.chamado.status === 'ABERTO') ? false : true}
                        rows="6"
                        type="textarea"
                        name="mensagem"
                        onChange={e => this.setState({ conteudoNovaMensagem: e.target.value })}
                        value={this.state.conteudoNovaMensagem}
                        placeholder={(this.state.chamado.status === 'ABERTO') ? 'Digite sua mensagem' : `Este chamado está ${this.state.chamado.status} e você não pode mais enviar mensagens`}
                      />
                    </div>
                  </form>
                </div>
                <Button className="float-right" disabled={(this.state.conteudoNovaMensagem && this.state.chamado.status === 'ABERTO') ? false : true} color="success mt-3 mb-3" onClick={this.atualizarConteudoMensagemEnviarMensagem}>Responder</Button>{' '}
              </Col>
            </Row>
          </Container>

        </div>

      </>
    )
  }
}