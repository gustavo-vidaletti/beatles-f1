import React from 'react'
import './Mensagem.css';

const MensagemUi = ( { listaMensagens } ) => 
    <>
        {
            listaMensagens && listaMensagens.map( e => 
                <div className="comment" key={ e.data }>
                    { <span className="avatar">
                        <img alt="avatar" src={e.gravatar}/>
                    </span> }
                    <div className="content">
                        <span className="author">
                            {e.username}
                        </span>
                        <div className="metadata">
                            <span className="date">{e.dataFormatada}</span>
                        </div>
                        <div className="text"><p>{e.conteudo}</p></div>
                    </div>
                </div>
            )
        }
    </>
; 

export default MensagemUi; 