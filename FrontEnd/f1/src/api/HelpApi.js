import axios from 'axios';
import md5 from 'md5';
import * as moment from 'moment';
import Mensagem from '../models/Mensagem';
import ChamadoDTO from '../models/ChamadoDTO';
import UsuarioLogado from '../models/UsuarioLogado';
import AnexoDTO from '../models/AnexoDTO'
moment.locale = 'pt-br'
const url = 'http://localhost:8080';
moment.locale = 'pt-br'
export default class HelpApi {

  async requestPadrao(urlToda, metodo = 'get', dados = '') {
    const tkn = localStorage.getItem(md5('token'))
    const resposta = await axios({
      method: metodo,
      url: urlToda,
      headers: {
        Authorization: tkn
      },
      data: dados
    }).catch(() => {
      return null
    })
    if (!resposta) return null
    return resposta.data
  }

  //REGISTRO
  async registrarUsuario(usuario){
    const urlToda = `${url}/f1/usuario/`
    const resposta = await axios({
      method: 'post',
      url: urlToda,
      data: usuario
    }).catch(() => {
      return null
    })
    if (!resposta) return null
    return resposta.data
  }

  async contemEmail(email){
    const u = `${url}/f1/usuario/contem/email/${email}`
    const resp = await axios({
      method: 'get',
      url: u
    })
    return resp.data;
  }

  async contemUsuario(user){
    const u = `${url}/f1/usuario/contem/username/${user}`
    const resp = axios({
      method: 'get',
      url: u
    })    
    return resp;
  }

  // LOGIN
  async login(username, password) {
    const login = await axios(
      {
        method: 'post',
        url: `${url}/login`,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          username: username,
          password: password
        }
      }).catch((error) => {
        return null;
      })
    if (!login) return null;
    localStorage.setItem(md5('token'), login.headers.authorization)
    return this.usuarioLogado()
  }

  // usuarioLogado
  async usuarioLogado() {
    const usuarioLogado = await this.requestPadrao(`${url}/f1/usuario/usuarioLogado`)
    if (!usuarioLogado) {
      localStorage.removeItem(md5('token'))
      localStorage.removeItem(md5('usuarioLogado'))
      return null
    }
    const dto = new UsuarioLogado(usuarioLogado.username, usuarioLogado.tipoUsuario, usuarioLogado.email, this.getGravatar(usuarioLogado.email))
    localStorage.setItem(md5('usuarioLogado'), JSON.stringify(dto))
    return dto
  }

  // Logout
  async logout() {
    localStorage.removeItem(md5('token'))
    localStorage.removeItem(md5('usuarioLogado'))
    return true;
  }

  async listarUsernames() {
    const usernames = await this.requestPadrao(`${url}/f1/chamado/usuarios/`)

    if (!usernames) return null

    return usernames
  }

  // Listar todos os chamados
  async listarChamados() {
    const chamados = await this.requestPadrao(`${url}/f1/chamado/`)
    if(!chamados){
      return []
    }
    return chamados.map((c) => {
      return (new ChamadoDTO(
        c.id,
        c.titulo,
        c.dataCriacao,
        moment(c.dataCriacao).format('DD/MM/YYYY - HH:mm'),
        c.dataLimite,
        moment(c.dataLimite).format('DD/MM/YYYY - HH:mm'),
        c.prioridade,
        c.username,
        c.status,
        c.descricao,
        c.anexos.map((a) => { return new AnexoDTO(a.id, a.url, a.url.split('\\')[a.url.split('\\').length - 1], a.chamado) }))
      )
    });
  }

  // Listar mensagens do chamado {ID}
  async getMensagensDoChamado(idChamado = "") {
    const mensagens = await this.requestPadrao(`${url}/f1/mensagem/chamado/${idChamado}`)
    if (!mensagens) return []
    return mensagens.map((m) => {
      return (new Mensagem(m.conteudo, m.data, moment(m.data).format('DD.MM.YYYY - HH:mm'), m.username, m.email, this.getGravatar(m.email)))
    })
  }

  async getDetalhesChamado(id) {
    const chamadoBanco = await this.requestPadrao(`${url}/f1/chamado/${id}`)
    if (!chamadoBanco) return null
    return new ChamadoDTO(
      chamadoBanco.id,
      chamadoBanco.titulo,
      chamadoBanco.dataCriacao,
      moment(chamadoBanco.dataCriacao).format('DD.MM.YYYY - HH:mm'),
      chamadoBanco.dataLimite,
      moment(chamadoBanco.dataLimite).format('DD.MM.YYYY - HH:mm'),
      chamadoBanco.prioridade,
      chamadoBanco.username,
      chamadoBanco.status,
      chamadoBanco.descricao,
      chamadoBanco.anexos.map((a) => { return new AnexoDTO(a.id, a.url, a.url.split('\\')[a.url.split('\\').length - 1], a.chamado) }))
  }

  async resolverChamado(id, mensagem = '') {
    const dados = {
      conteudo: mensagem
    }
    const resolver = await this.requestPadrao(`${url}/f1/chamado/${id}/resolver`, 'patch', dados)
    return (resolver) ? true : false
  }

  async arquivarChamado(id, mensagem = '') {
    const dados = {
      conteudo: mensagem
    }
    const arquivar = await this.requestPadrao(`${url}/f1/chamado/${id}/arquivar`, 'patch', dados)
    return (arquivar) ? true : false
  }

  async responderChamado(idChamado, conteudo) {
    const dados = {
      conteudo: conteudo
    }
    const responder = await this.requestPadrao(`${url}/f1/mensagem/chamado/${idChamado}`, 'post', dados)
    //return responder
    if (!responder) {
      return null
    }
    return new Mensagem(responder.conteudo, responder.data, moment(responder.data).format('DD.MM.YYYY - HH:mm'), responder.username, responder.email, this.getGravatar(responder.email))
  }

  async novoChamado(chamado, anexo = []) {
    const method = (chamado.id) ? 'put' : 'post'
    const chamadoBanco = this.requestPadrao(`${url}/f1/chamado/${(chamado.id) ? chamado.id : '' }`, method, chamado)
    if (!chamadoBanco) {
      return null
    }

    anexo.forEach(async (a) => {
      const anexo = await this.novoAnexo(a)
      if (!anexo) return null;
    })
    return chamadoBanco
  }

  async novoAnexo(anexo, idChamado) {
    const tkn = localStorage.getItem(md5('token'))
    axios({
      method: 'post',
      url: `${url}/f1/anexo/${idChamado}`,
      headers: {
        Authorization: tkn,
        'Content-Type': 'multipart/form-data'
      },
      data: anexo
    }).then((response) => {
      return response.data
    }).catch(err => {
      return null;
    })
  }

  async novoAnexoByFile(anexo, idChamado) {
    let data = new FormData();
    data.append('file', anexo);
    const tkn = localStorage.getItem(md5('token'))
    axios({
      method: 'post',
      url: `${url}/f1/anexo/${idChamado}`,
      headers: {
        Authorization: tkn,
        'Content-Type': 'multipart/form-data'
      },
      data: data
    }).then((response) => {
      return response.data
    }).catch(err => {
      return null;
    })
  }

  async downloadAnexo(id, nome = "") {
    const tkn = localStorage.getItem(md5('token'))
    axios({
      method: 'get',
      responseType: 'blob',
      url: `${url}/f1/anexo/download/${id}`,
      headers: {
        Authorization: tkn
      }
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', nome);
      document.body.appendChild(link);
      link.click();
    }).catch(err => {
      return null;
    })
  }

  getGravatar(email) {
    return `https://www.gravatar.com/avatar/${md5(email)}?s=150&d=robohash`
  }


}