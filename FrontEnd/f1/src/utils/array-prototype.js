/* eslint-disable no-extend-native */
import * as moment from 'moment'

Array.prototype.ordenarMensagens = function(){
    return this.sort( (a, b) => {
        return moment(a.data).diff(b.data)
    })
}

Array.prototype.ordenarChamados = function(){
    function ordemPrioridade(pri){
        switch(pri){
            case 'ALTA': return 3;
            case 'NORMAL': return 2;
            case 'BAIXA': return 1;
            default: return 0;
        }
    }
    return this.sort( (a, b) => {
        moment.locale = 'pt-br'
        if( moment(a.dataLimite).format('x') === moment(b.dataLimite).format('x') ){
            return ordemPrioridade(b.prioridade) - ordemPrioridade(a.prioridade)
        }
        return (moment(a.dataLimite).diff(b.dataLimite))
    } )
}

Array.prototype.filtrarPorUsername = function(username){
    return this.filter( (chamadoDTO) => {
        return (chamadoDTO.username === username)
    })
}

Array.prototype.filtrarPorStatus = function(status){
    return this.filter( (chamadoDTO) => {
        return (chamadoDTO.status === status)
    })
}