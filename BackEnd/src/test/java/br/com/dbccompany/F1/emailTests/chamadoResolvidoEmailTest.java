package br.com.dbccompany.F1.emailTests;

import br.com.dbccompany.F1.F1Application;
import br.com.dbccompany.F1.email.EmailController;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Mensagem;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.Prioridade;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = F1Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class chamadoResolvidoEmailTest {

    @Autowired
    EmailController emailController;

    @Test
    public void testarChamadoResolvidoEmailEnviadoParaCriador() {

        Usuario usuario1 = new Usuario();
        usuario1.setTipoUsuario(TipoUsuario.OPERADOR);

        Usuario usuario2 = new Usuario();
        usuario2.setEmail("igor.ceriotti@dbccompany.com.br");
        usuario2.setTipoUsuario(TipoUsuario.COMUM);

        Chamado chamado = new Chamado();
        chamado.setDescricao("teste criacao chamado");
        chamado.setPrioridade(Prioridade.ALTA);
        chamado.setTitulo("teste criacao chamado");
        chamado.setUsuario(usuario2);

        Mensagem mensagem = new Mensagem();
        mensagem.setChamado(chamado);
        mensagem.setUsuario(usuario1);
        mensagem.setConteudo("Eu, operador, resolvi seu chamado!");

        boolean condicao = emailController.mensagemEnviada(mensagem);
        assertTrue(condicao);

    }

}
