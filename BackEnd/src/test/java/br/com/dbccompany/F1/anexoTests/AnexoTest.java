package br.com.dbccompany.F1.anexoTests;

import br.com.dbccompany.F1.F1Application;
import br.com.dbccompany.F1.controller.AnexoController;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.Prioridade;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.exceptions.FileLimitExceededException;
import br.com.dbccompany.F1.service.ChamadoService;
import br.com.dbccompany.F1.service.UsuarioService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@SpringBootTest(classes = F1Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@Transactional
public class AnexoTest {
        @Autowired
        private UsuarioService usuarioService;

        @Autowired
        private AnexoController controller = new AnexoController();

        @Autowired
        private ChamadoService chamadoService = new ChamadoService();

        @Test
        public void naoPodeAdiconarMaisDe3Anexos() throws Exception {

            Usuario usuario = new Usuario();
            usuario.setId(1);
            usuario.setNome("Marlon Silva");
            usuario.setUsername("marlon.silva");
            usuario.setEmail("marlon.silva@dbccompany.com.br");
            usuario.setPassword("1234567");
            usuario.setTipoUsuario(TipoUsuario.COMUM);
            try {
                usuarioService.salvar(usuario);
            } catch (Exception e){}

            Chamado chamado = new Chamado();
            chamado.setId(1);
            chamado.setTitulo("Chamado test");
            chamado.setDescricao("Chamado para testar anexo");
            chamado.setPrioridade(Prioridade.ALTA);
            chamado.setUsuario(usuario);
            try {
                chamadoService.salvar(chamado);
            } catch (Exception e) {}

            MockMultipartFile mockMultipartFile = new MockMultipartFile("file", "excel.xlsx", "multipart/form-data", new byte[10]);
            MockMultipartFile mockMultipartFile2 = new MockMultipartFile("file", "oi.txt", "plain/text", new byte[6]);
            MockMultipartFile mockMultipartFile3 = new MockMultipartFile("file", "tchau.pdf", "multipart/form-data", new byte[4]);

            controller.adicionar(mockMultipartFile, 1);
            controller.adicionar(mockMultipartFile2, 1);
            controller.adicionar(mockMultipartFile3, 1);

            try {
                MockMultipartFile mockMultipartFile4 = new MockMultipartFile("file", "hello.doc", "multipart/form-data", new byte[15]);
                controller.adicionar(mockMultipartFile4, 1);
            } catch (Exception e){
                Assert.assertEquals(FileLimitExceededException.class, e.getClass());
            }

    }}




