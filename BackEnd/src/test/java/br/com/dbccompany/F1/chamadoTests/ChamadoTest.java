package br.com.dbccompany.F1.chamadoTests;

import br.com.dbccompany.F1.F1Application;
import br.com.dbccompany.F1.controller.ChamadoController;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.Prioridade;
import br.com.dbccompany.F1.enumeration.Status;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.service.ChamadoService;
import br.com.dbccompany.F1.service.UsuarioService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest(classes = F1Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@Transactional
public class ChamadoTest {

    @Autowired
    private ChamadoService chamadoService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ChamadoController chamadoController;

    @Test
    public void testarMudancaDeStatusParaAberto(){
        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("José");
        usuario.setUsername("josezinho");
        usuario.setEmail("josezinho@dbccompany.com.br");
        usuario.setPassword("1234567");
        usuario.setTipoUsuario(TipoUsuario.COMUM);
        try {
            usuarioService.salvar(usuario);
        } catch (Exception e){}

        Chamado chamado = new Chamado();
        chamado.setId(1);
        chamado.setTitulo("Chamado test");
        chamado.setDescricao("Chamado para testar");
        chamado.setPrioridade(Prioridade.ALTA);
        chamado.setUsuario(usuario);
        try {
            chamadoService.salvar(chamado);
        } catch (Exception e) {}

        Status status = Status.ABERTO;

        Chamado found = chamadoService.buscarPorStatus(status);

        assertEquals(status, found.getStatus());
    }

    @Test
    public void testarMudancaDeStatusParaResolvido(){
        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("José");
        usuario.setUsername("josezinho");
        usuario.setEmail("josezinho@dbccompany.com.br");
        usuario.setPassword("1234567");
        usuario.setTipoUsuario(TipoUsuario.COMUM);
        try {
            usuarioService.salvar(usuario);
        } catch (Exception e){}

        Chamado chamado = new Chamado();
        chamado.setId(1);
        chamado.setTitulo("Chamado test");
        chamado.setDescricao("Chamado para testar");
        chamado.setPrioridade(Prioridade.ALTA);
        chamado.setUsuario(usuario);
        try {
            Status statusFound = chamadoController.resolver(1, null).getStatus();
            Status status = Status.RESOLVIDO;
            assertEquals(status, statusFound);
        } catch (Exception e){}
    }

    @Test
    public void testarMudancaDeStatusParaArquivado(){
        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("José");
        usuario.setUsername("josezinho");
        usuario.setEmail("josezinho@dbccompany.com.br");
        usuario.setPassword("1234567");
        usuario.setTipoUsuario(TipoUsuario.COMUM);
        try {
            usuarioService.salvar(usuario);
        } catch (Exception e){}

        Chamado chamado = new Chamado();
        chamado.setId(1);
        chamado.setTitulo("Chamado test");
        chamado.setDescricao("Chamado para testar");
        chamado.setPrioridade(Prioridade.ALTA);
        chamado.setUsuario(usuario);
        try {
            Status statusFound = chamadoController.arquivar(1, null, null).getStatus();
            Status status = Status.ARQUIVADO;
            assertEquals(status, statusFound);
        } catch (Exception e){}
    }

    @Test
    public void testarCamposObrigatorios(){
        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("José");
        usuario.setUsername("josezinho");
        usuario.setEmail("josezinho@dbccompany.com.br");
        usuario.setPassword("1234567");
        usuario.setTipoUsuario(TipoUsuario.COMUM);
        try {
            usuarioService.salvar(usuario);
        } catch (Exception e){}

        Chamado chamado = new Chamado();
        chamado.setId(1);
        chamado.setDescricao("Chamado para testar");
        chamado.setPrioridade(Prioridade.ALTA);
        chamado.setUsuario(usuario);

        try {
            chamadoService.salvar(chamado);
        } catch (Exception e) {
            assertEquals(Exception.class, e.getClass());
        }
    }
}
