package br.com.dbccompany.F1.emailTests;

import br.com.dbccompany.F1.F1Application;
import br.com.dbccompany.F1.email.EmailController;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Mensagem;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.Prioridade;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = F1Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class chamadoArquivadoEmailTest {

    @Autowired
    EmailController emailController;


    @Test
    public void testarChamadoArquivadoEmailParaOperador() {

        Usuario usuario = new Usuario();
        usuario.setTipoUsuario(TipoUsuario.OPERADOR);

        Usuario usuario1 = new Usuario();
        usuario1.setEmail("igor.ceriotti@dbccompany.com.br");
        usuario1.setTipoUsuario(TipoUsuario.COMUM);

        Chamado chamado = new Chamado();
        chamado.setDescricao("teste criacao chamado");
        chamado.setPrioridade(Prioridade.ALTA);
        chamado.setTitulo("teste criacao chamado");
        chamado.setUsuario(usuario1);

        Mensagem mensagem = new Mensagem();
        mensagem.setChamado(chamado);
        mensagem.setUsuario(usuario1);
        mensagem.setConteudo("Chamado arquivado!");

        boolean condicao = emailController.chamadoArquivado(mensagem);
        assertTrue(condicao);

    }

    @Test
    public void testarErroAoTentarArquivarChamadoQuandoNaoEhOCriador() {

        Usuario usuario = new Usuario();
        usuario.setTipoUsuario(TipoUsuario.OPERADOR);

        Usuario usuario1 = new Usuario();
        usuario1.setEmail("igor.ceriotti@dbccompany.com.br");
        usuario1.setTipoUsuario(TipoUsuario.COMUM);

        Chamado chamado = new Chamado();
        chamado.setDescricao("teste criacao chamado");
        chamado.setPrioridade(Prioridade.ALTA);
        chamado.setTitulo("teste criacao chamado");
        chamado.setUsuario(usuario1);

        Mensagem mensagem = new Mensagem();
        mensagem.setChamado(chamado);
        mensagem.setUsuario(usuario);
        mensagem.setConteudo("Estou tentando arquivar um chamado que não criei!");

        boolean condicao = emailController.chamadoArquivado(mensagem);
        assertFalse(condicao);

    }

}
