package br.com.dbccompany.F1.email;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class EnviarEmail implements Runnable{
    private String emails[];
    private String assunto;
    private String conteudo;
    private JavaMailSender emailSender;

    public EnviarEmail(String[] emails, String assunto, String conteudo, JavaMailSender emailSender) {
        this.emailSender = emailSender;
        this.emails = emails;
        this.assunto = assunto;
        this.conteudo = conteudo;
    }

    @Override
    public void run() {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
            helper.setTo(this.emails);

            helper.setSubject(this.assunto);
            message.setContent(this.conteudo, "text/html; charset=utf-8");
            this.emailSender.send(message);
        } catch (MessagingException e) {
            System.err.println("ERRO AO ENVIAR EMAIL!");
        }

    }
}
