package br.com.dbccompany.F1.email;

import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Mensagem;

import java.text.SimpleDateFormat;
import java.util.Arrays;

public class MyConstants {

    public static final String EMAIL_OPERADOR = "operadordof1@gmail.com";

    public static final String MY_PASSWORD = "Srnbb5mG";

    public static String assuntoEmail(String assunto, Chamado chamado){
        return "[F1 - " + assunto + "] " + idString(chamado.getId()) + " - " + chamado.getTitulo();
    }

    public static String[] emailsTo(String... to){
        return (String[]) Arrays.asList(to).toArray();
    }

    public static SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy - HH:mm");

    public static String conteudoCriacao(Chamado chamado) {
        StringBuffer sb = new StringBuffer();
        sb.append("<h3>Olá,</h3>");
        sb.append("<h4>O chamado [ " + idString(chamado.getId())  + " - " + chamado.getTitulo() +
                " ] foi criado com as seguintes informações: </h4>");
        sb.append("<div>" +
                "<b>Descrição:</b>" +
                "<p><i>\"" + chamado.getDescricao() + "\"</i></p>" +
                "<b>Data de criação:</b> " + formato.format(chamado.getDataCriacao()) + "<br>" +
                "<b>Data limite:</b> " + formato.format(chamado.getDataLimite()) + "<br>" +
                "<b>Prioridade:</b> " + chamado.getPrioridade() + "</div>");
        sb.append("<h4>Clique " +
                //TODO: LINK DO FRONTEND
                "<a href=\"http://localhost:3000/detalhe/" + chamado.getId() +"\">aqui</a> para abrí-lo" +
                "</h4>");
        return sb.toString();
    }

    public static String conteudoResposta(Mensagem mensagem){
        StringBuffer sb = new StringBuffer();
        sb.append("<h3>Olá,</h3>");
        sb.append("<h4>O chamado [ " + idString(mensagem.getChamado().getId())  + " - " + mensagem.getChamado().getTitulo() +
                " ] foi respondido: </h4>");
        sb.append("<div>" +
                "<p><i>\"" + mensagem.getConteudo() + "\"</i></p>" +
                "<b>" + formato.format(mensagem.getData()) +"</b>" +
                "</div>");

        sb.append("<h4>Clique " +
                //TODO: LINK DO FRONTEND
                "<a href=\"http://localhost:3000/detalhe/" + mensagem.getChamado().getId() + "\">aqui</a> para abrí-lo" +
                "</h4>");
        return sb.toString();
    }

    public static String conteudoArquivado(Mensagem mensagem){
        StringBuffer sb = new StringBuffer();
        sb.append("<h3>Olá,</h3>");
        sb.append("<h4>O chamado [ " + idString(mensagem.getChamado().getId())  + " - " + mensagem.getChamado().getTitulo() +
                " ] foi arquivado: </h4>");
        if(!mensagem.getConteudo().equals("")){
            sb.append("<div>" +
                    "<p><i>\"" + mensagem.getConteudo() + "\"</i></p>" +
                    "<b>" + formato.format(mensagem.getData()) +"</b>" +
                    "</div>");
        }

        sb.append("<h4>Clique " +
                //TODO: LINK DO FRONTEND
                "<a href=\"http://localhost:3000/detalhe/" + mensagem.getChamado().getId() + "\">aqui</a> para abrí-lo" +
                "</h4>");
        return sb.toString();
    }

    public static String conteudoResolvido(Mensagem mensagem){
        StringBuffer sb = new StringBuffer();
        sb.append("<h3>Olá,</h3>");
        sb.append("<h4>O chamado [ " + idString(mensagem.getChamado().getId())  + " - " + mensagem.getChamado().getTitulo() +
                " ] foi resolvido: </h4>");
        sb.append("<div>" +
                "<p><i>\"" + mensagem.getConteudo() + "\"</i></p>" +
                "<b>" + formato.format(mensagem.getData()) +"</b>" +
                "</div>");


        sb.append("<h4>Clique " +
                //TODO: LINK DO FRONTEND
                "<a href=\"http://localhost:3000/detalhe/" + mensagem.getChamado().getId() + "\">aqui</a> para abrí-lo" +
                "</h4>");
        return sb.toString();
    }

    private static String idString(long id){
        String idString = String.valueOf(id);
        if(id < 10) {
            idString = "0" + id;
        }
        return idString;
    }
}
