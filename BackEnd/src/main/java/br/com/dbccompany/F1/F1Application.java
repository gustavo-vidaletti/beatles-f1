package br.com.dbccompany.F1;

import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.properties.StorageProperties;
import br.com.dbccompany.F1.service.UsuarioService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)

public class F1Application {

	public static void main(String[] args) {
		SpringApplication.run(F1Application.class, args);
	}

	@Bean
	public CommandLineRunner demo(UsuarioService service){
		if(service.buscarPorEmail("operadordof1@gmail.com") == null){
			Usuario principal = new Usuario();
			principal.setEmail("operadordof1@gmail.com");
			principal.setUsername("operador");
			principal.setNome("Operador F1");
			principal.setPassword("dbccompany");
			principal.setTipoUsuario(TipoUsuario.OPERADOR);
			return args -> {
				service.salvar(principal);
			};
		}
		return args -> {};
	}

}
