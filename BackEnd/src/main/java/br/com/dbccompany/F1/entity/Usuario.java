package br.com.dbccompany.F1.entity;

import br.com.dbccompany.F1.dto.UsuarioLogadoDTO;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Usuario.class)
public class Usuario {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "usuario_seq", sequenceName = "usuario_seq")
    @GeneratedValue(generator = "usuario_seq", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(nullable = false)
    private String nome;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(unique = true, nullable = false)
    private String username;

    @Column( nullable = false)
    private String password;

    @Column(name = "tipo_usuario", nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoUsuario tipoUsuario = TipoUsuario.COMUM;

    @OneToMany(mappedBy = "usuario", cascade = CascadeType.MERGE)
    private List<Mensagem> mensagens = new ArrayList<>();

    @OneToMany(mappedBy = "usuario", cascade = CascadeType.MERGE)
    private List<Chamado> chamados = new ArrayList<>();

    public Usuario() {
    }

    public Usuario(String nome, String email, String username, String password, TipoUsuario tipoUsuario) {
        this.nome = nome;
        this.email = email;
        this.username = username;
        this.password = password;
        this.tipoUsuario = tipoUsuario;
    }

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username.toLowerCase();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public List<Mensagem> getMensagens() {
        return mensagens;
    }

    public void setMensagens(List<Mensagem> mensagens) {
        this.mensagens = mensagens;
    }

    public void pushMensagens(Mensagem... mensagens){
        this.mensagens.addAll(Arrays.asList(mensagens));
    }

    public List<Chamado> getChamados() {
        return chamados;
    }

    public void setChamados(List<Chamado> chamados) {
        this.chamados = chamados;
    }

    public void pushChamados(Chamado... chamados){
        this.chamados.addAll(Arrays.asList(chamados));
    }

    public UsuarioLogadoDTO geraUsuarioDTO(){
        return new UsuarioLogadoDTO(this.username, this.tipoUsuario, this.email);
    }

}
