package br.com.dbccompany.F1.controller;

import br.com.dbccompany.F1.dto.ChamadoDTO;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.Status;
import br.com.dbccompany.F1.email.EmailController;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Mensagem;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.exceptions.UserNotFoundException;
import br.com.dbccompany.F1.security.TokenAuthenticationService;
import br.com.dbccompany.F1.service.ChamadoService;
import br.com.dbccompany.F1.service.MensagemService;
import br.com.dbccompany.F1.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/f1/chamado")
public class ChamadoController {

    @Autowired
    ChamadoService chamadoService;

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    EmailController emailController;

    @Autowired
    MensagemService mensagemService;

    @PreAuthorize("hasAnyRole('ROLE_OPERADOR', 'ROLE_COMUM')")
    @GetMapping( value = "/" )
    public List<ChamadoDTO> listarTodos(HttpServletRequest http){
        List<Chamado> lista = listarPrivado(http);
        List<ChamadoDTO> listaDTO = new ArrayList<>();
        for(Chamado chamado: lista){
            listaDTO.add(chamado.geraDTO());
        }
        return listaDTO;
    }

    @PreAuthorize("hasRole('ROLE_OPERADOR')")
    @GetMapping( value = "/usuarios/")
    public List<String> listarUsuariosDeTodosChamados(){
        List<String> listaDeNomes = new ArrayList<>();
        for(Chamado chamado: this.chamadoService.listarTodos()){
            if(!listaDeNomes.contains(chamado.getUsuario().getUsername())){
                listaDeNomes.add(chamado.getUsuario().getUsername());
            }
        }
        return listaDeNomes;
    }

    private List<Chamado> listarPrivado(HttpServletRequest http){
        String username = TokenAuthenticationService.getUsername(http);
        Usuario usuario = usuarioService.buscarPorUsername(username);
        if(usuario.getTipoUsuario() != TipoUsuario.OPERADOR){
            return chamadoService.buscarPorUsuario(usuario.getId());

        }
        return chamadoService.listarTodos();
    }

    @PreAuthorize("hasAnyRole('ROLE_OPERADOR', 'ROLE_COMUM')")
    @GetMapping( value = "/{id}" )
    public ChamadoDTO buscar(@PathVariable long id, HttpServletRequest http)  {
        Chamado chamado = chamadoService.buscarPorId(id);

        String username = TokenAuthenticationService.getUsername(http);
        Usuario usuario = usuarioService.buscarPorUsername(username);

        if(usuario.getTipoUsuario() != TipoUsuario.OPERADOR &&
            usuario.getId() != chamado.getUsuario().getId()){
            return null;
        }
        return chamado.geraDTO();
    }

    @PreAuthorize("hasAnyRole('ROLE_OPERADOR', 'ROLE_COMUM')")
    @PostMapping( value = "/" )
    public Chamado adicionar( @RequestBody Chamado chamado, HttpServletRequest http) throws Exception {
        String username = TokenAuthenticationService.getUsername(http);
        Usuario usuario = usuarioService.buscarPorUsername(username);
        chamado.setUsuario(usuario);

        if(usuario == null){
            throw new UserNotFoundException("Não obtivemos informacao suficiente!");
        }

        if(chamado.getDataCriacao() == null) chamado.setDataCriacao(new Date((System.currentTimeMillis())));
        chamado.setUsuario(usuario);
        chamado = chamadoService.salvar(chamado);
        emailController.chamadoCriado(chamado);
        return chamado;
    }

    @PreAuthorize("hasAnyRole('ROLE_OPERADOR', 'ROLE_COMUM')")
    @PutMapping( value = "/{id}" )
    public ChamadoDTO editar( @PathVariable long id, @RequestBody ChamadoDTO chamadoDTO, HttpServletRequest http) throws Exception {
        String usernameTkn = TokenAuthenticationService.getUsername(http);
        Usuario usuario = usuarioService.buscarPorUsername(usernameTkn);

        Chamado chamado = chamadoService.buscarPorId(id);

        if(usuario.getId() != chamado.getUsuario().getId()){
            throw new AccessDeniedException("Não tem permissão para editar este chamado!");
        }

        chamado.setDescricao(chamadoDTO.getDescricao());
        chamado.setTitulo(chamadoDTO.getTitulo());
        chamado.setStatus(chamadoDTO.getStatus());
        chamado.setPrioridade(chamadoDTO.getPrioridade());
        if(chamadoDTO.getDataLimite() != null && chamadoDTO.getDataLimite() != chamado.getDataLimite()){
            chamado.setDataLimite(chamadoDTO.getDataLimite());
        }

        return chamadoService.editarPorId(id, chamado).geraDTO();
    }

    @PreAuthorize("hasAnyRole('ROLE_COMUM','ROLE_OPERADOR')")
    @PatchMapping(value = "/{id}/arquivar" )
    public Chamado arquivar(@PathVariable long id, @RequestBody(required = false) Mensagem mensagemFinal, HttpServletRequest http) throws Exception{
        Chamado chamado = chamadoService.buscarPorId(id);
        String username = TokenAuthenticationService.getUsername(http);
        if(!username.equals(chamado.getUsuario().getUsername())){
            throw new AccessDeniedException("Não tem permissão para arquivar esse chamado!");
        }

        chamado.setStatus(Status.ARQUIVADO);

        if(mensagemFinal.getConteudo().equals("")){
            mensagemFinal = new Mensagem();
            mensagemFinal.setConteudo("Este chamado foi arquivado");
        }

        mensagemFinal.setChamado(chamado);
        mensagemFinal.setUsuario(chamado.getUsuario());
        mensagemFinal = mensagemService.salvar(mensagemFinal);


        emailController.chamadoArquivado(mensagemFinal);
        return chamadoService.salvar(chamado);
    }

    @PreAuthorize("hasRole('ROLE_OPERADOR')")
    @PatchMapping(value = "/{id}/resolver")
    public Chamado resolver(@PathVariable long id, @RequestBody(required = false) Mensagem mensagemFinal) throws Exception{
        Chamado chamado = chamadoService.buscarPorId(id);
        chamado.setStatus(Status.RESOLVIDO);

        if((mensagemFinal == null || mensagemFinal.getConteudo().equals(""))){
            mensagemFinal = new Mensagem();
            mensagemFinal.setConteudo("Este chamado foi resolvido");
        }

        mensagemFinal.setChamado(chamado);
        mensagemFinal.setUsuario(usuarioService.buscarTodosPorTipo(TipoUsuario.OPERADOR).get(0));
        mensagemFinal = mensagemService.salvar(mensagemFinal);

        emailController.chamadoResolvido(mensagemFinal);
        return chamadoService.salvar(chamado);
    }
}
