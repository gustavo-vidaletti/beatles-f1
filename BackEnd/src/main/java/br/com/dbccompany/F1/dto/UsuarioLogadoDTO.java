package br.com.dbccompany.F1.dto;

import br.com.dbccompany.F1.enumeration.TipoUsuario;

public class UsuarioLogadoDTO {

    String username;
    TipoUsuario tipoUsuario;
    String email;

    public UsuarioLogadoDTO(String username, TipoUsuario tipoUsuario, String email) {
        this.username = username;
        this.tipoUsuario = tipoUsuario;
        this.email = email;
    }

    // Getters and Setters

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
