package br.com.dbccompany.F1.exceptions;

public class EmailNotValidException extends RuntimeException {

    public EmailNotValidException(String message, Throwable cause){
        super(message, cause);
    }

    public EmailNotValidException(String message){
        super(message);
    }
}
