package br.com.dbccompany.F1.verification;

import br.com.dbccompany.F1.enumeration.TipoUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.List;

public class Permissoes {

    public static List<GrantedAuthority> definirRoles(TipoUsuario TipoUsuario){
        List<GrantedAuthority> permissoes;

        switch (TipoUsuario) {
            case OPERADOR:
                permissoes = AuthorityUtils.createAuthorityList("ROLE_OPERADOR");
                break;
            case COMUM:
                permissoes = AuthorityUtils.createAuthorityList("ROLE_COMUM");
                break;
            case INATIVO:
                permissoes = AuthorityUtils.createAuthorityList("ROLE_INATIVO");
                break;
            default:
                permissoes = AuthorityUtils.createAuthorityList("ROLE_DEFAULT");
        }

        return permissoes;
    }

}
