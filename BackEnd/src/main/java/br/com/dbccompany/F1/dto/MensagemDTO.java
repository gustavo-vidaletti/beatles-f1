package br.com.dbccompany.F1.dto;

import java.util.Date;

public class MensagemDTO {

    public MensagemDTO(String conteudo, Date data, String username, String email){
        this.conteudo = conteudo;
        this.data = data;
        this.username = username;
        this.email = email;
    }

    String conteudo;

    Date data;

    String username;

    String email;

    // Getters and Setters

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
