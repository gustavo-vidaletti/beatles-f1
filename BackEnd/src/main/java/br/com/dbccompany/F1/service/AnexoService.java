package br.com.dbccompany.F1.service;

import br.com.dbccompany.F1.entity.Anexo;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.exceptions.StorageFileNotFoundException;
import br.com.dbccompany.F1.exceptions.StorageException;
import br.com.dbccompany.F1.properties.StorageProperties;
import br.com.dbccompany.F1.repository.AnexoRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.*;
import java.util.List;
import java.util.stream.Stream;


@Service
public class AnexoService {

    @Autowired
    private AnexoRepositoy anexoRepositoy;


    @Autowired
    private ChamadoService chamadoService;

    private final Path rootLocation;

    @Autowired
    public AnexoService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Transactional(rollbackFor = Exception.class)
    public Anexo salvar(Anexo anexo) throws Exception{
        return anexoRepositoy.save(anexo);
    }

    @Transactional(rollbackFor = Exception.class)
    public String store(MultipartFile file, long idChamado) throws Exception{
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Arquivo vazio " + filename);
            }
            if (filename.contains("..")) {
                // checagem de segurança
                throw new StorageException(
                        "Não foi possivel salvar o arquivo "
                                + filename);
            }
            File folder = new File(this.rootLocation.toAbsolutePath().toString());
            folder.mkdir();
            folder = new File(folder.getAbsolutePath() + "\\" + idChamado);
            folder.mkdir();
            File arquivoComMesmoNome = new File(folder.getAbsolutePath()  + "\\" + filename);

            if(arquivoComMesmoNome.exists()){
                throw new FileAlreadyExistsException("Este arquivo já está salvo");
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, folder.toPath().resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        }
        catch (IOException e) {
            throw new StorageException("Não foi possivel salvar o arquivo " + filename, e);
        }
        return this.rootLocation.toAbsolutePath().toString() + "\\" + idChamado + "\\" + file.getOriginalFilename();
    }

    public List<Anexo> listarTodos() {
        return anexoRepositoy.findAll();
    }

    public Anexo buscarPorId(long id){
        return anexoRepositoy.findById(id);
    }

    public List<Anexo> buscarTodosPorChamado(long id ){
        Chamado chamado = chamadoService.buscarPorId(id);
        return  anexoRepositoy.findAllByChamado(chamado);
    }

    @Transactional(rollbackFor = Exception.class)
    public Anexo editar(long id, MultipartFile file) throws Exception{
        Anexo anexo = anexoRepositoy.findById(id);
        Files.deleteIfExists(Paths.get(anexo.getUrl()));
        String url = this.store(file, anexo.getChamado().getId());
        anexo.setUrl(url);
        return anexoRepositoy.save(anexo);
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean deletar(long id) throws Exception{
        Anexo anexo = anexoRepositoy.findById(id);
        boolean existia =  anexo != null;
        Files.deleteIfExists(Paths.get(anexo.getUrl()));
        anexoRepositoy.deleteById(id);
        return existia;
    }

}
