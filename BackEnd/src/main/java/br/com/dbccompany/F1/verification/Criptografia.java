package br.com.dbccompany.F1.verification;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Criptografia {

    public static String criptografarSenha(String senha){
        return new BCryptPasswordEncoder(6). encode(senha);
    }

    public static BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder(6);
    }
    
}
