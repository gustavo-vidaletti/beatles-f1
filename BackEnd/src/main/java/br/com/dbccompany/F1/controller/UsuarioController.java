package br.com.dbccompany.F1.controller;

import br.com.dbccompany.F1.dto.UsuarioLogadoDTO;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.security.TokenAuthenticationService;
import br.com.dbccompany.F1.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/f1/usuario")
@RestController
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @PreAuthorize("hasRole('ROLE_OPERADOR')")
    @GetMapping(value = "/")
    public List<Usuario> listarTodos(){
        return usuarioService.listarTodos();
    }

    @GetMapping(value = "/contem/username/{param}")
    public boolean contemUsuario(@PathVariable String param){
        return (usuarioService.buscarPorUsername(param.toLowerCase()) != null);
    }

    @GetMapping(value = "/contem/email/{param}")
    public boolean contemEmail(@PathVariable String param){
        return (usuarioService.buscarPorEmail(param) != null);
    }


    @GetMapping(value = "/{param}")
    public Usuario buscar(@PathVariable String param){
        try{
            long id = Long.parseLong(param);
            return usuarioService.buscarPorId(id);
        } catch (NumberFormatException erro){
            return usuarioService.buscarPorEmail(param);
        }
    }

    @GetMapping(value = "/usuarioLogado")
    public UsuarioLogadoDTO usuarioLogado(HttpServletRequest http){
            String username = TokenAuthenticationService.getUsername(http);
        Usuario usuario = usuarioService.buscarPorUsername(username);
        return usuario.geraUsuarioDTO();
    }


    @PostMapping(value = "/")
    public Usuario adicionar(@RequestBody Usuario usuario, HttpServletRequest http) throws Exception{
        if(usuario.getTipoUsuario() == TipoUsuario.OPERADOR){
            String username = TokenAuthenticationService.getUsername(http);
            if(username == null || username.equals("")){
                throw new AccessDeniedException("Não tem permissão para esta operação");
            }
            Usuario usuarioLogado = usuarioService.buscarPorUsername(username);
            if(usuarioLogado.getTipoUsuario() != TipoUsuario.OPERADOR){
                throw new AccessDeniedException("Não tem permissão para esta operação");
            }
        }
        return usuarioService.salvar(usuario);
    }

    @PutMapping(value = "/{id}")
    public Usuario editar(@PathVariable long id, @RequestBody Usuario usuario) throws Exception{
        return usuarioService.editarPorId(id, usuario);
    }

    @DeleteMapping(value = "/{id}")
    public Usuario deletar(@PathVariable long id) throws Exception{
        Usuario usuario = usuarioService.buscarPorId(id);
        usuario.setTipoUsuario(TipoUsuario.INATIVO);
        usuarioService.salvar(usuario);
        return usuario;
    }

}
