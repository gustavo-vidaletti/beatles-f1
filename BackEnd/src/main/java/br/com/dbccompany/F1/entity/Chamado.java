package br.com.dbccompany.F1.entity;

import br.com.dbccompany.F1.dto.AnexoDTO;
import br.com.dbccompany.F1.dto.ChamadoDTO;
import br.com.dbccompany.F1.enumeration.Prioridade;
import br.com.dbccompany.F1.enumeration.Status;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Chamado.class)
public class Chamado {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "chamado_seq", sequenceName = "chamado_seq")
    @GeneratedValue(generator = "chamado_seq", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(nullable = false)
    private String titulo;

    @Column(nullable = false)
    private String descricao;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Status status = Status.ABERTO;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Prioridade prioridade = Prioridade.NORMAL;

    @Column(name = "data_criacao")
    private Date dataCriacao = new Date(System.currentTimeMillis());

    @Column(name = "data_limite")
    private Date dataLimite = new Date(System.currentTimeMillis() + (10 * 24 * 60 * 60 * 1000));

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_criador" )
    private Usuario usuario;

    @Transient
    private String username;

    @OneToMany(mappedBy = "chamado", cascade = CascadeType.MERGE)
    private List<Anexo> anexos = new ArrayList<>();

    @OneToMany(mappedBy = "chamado", cascade = CascadeType.MERGE)
    private List<Mensagem> mensagens = new ArrayList<>();

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Prioridade getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(Prioridade prioridade) {
        this.prioridade = prioridade;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Date getDataLimite() {
        return dataLimite;
    }

    public void setDataLimite(Date dataLimite) {
        this.dataLimite = dataLimite;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<Anexo> anexos) {
        this.anexos = anexos;
    }

    public void pushAnexos(Anexo... anexos){
        this.anexos.addAll(Arrays.asList(anexos));
    }

    public List<Mensagem> getMensagens() {
        return mensagens;
    }

    public void setMensagens(List<Mensagem> mensagens) {
        this.mensagens = mensagens;
    }

    public void pushMensagens(Mensagem... mensagens){
        this.mensagens.addAll(Arrays.asList(mensagens));
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ChamadoDTO geraDTO(){
        List<AnexoDTO> anexoDTOS = new ArrayList<>();
        for(Anexo anexo: this.anexos){
            anexoDTOS.add(anexo.geraDTO());
        }
        return new ChamadoDTO(this.id,this.titulo, this.descricao, this.dataCriacao, this.dataLimite, this.prioridade, this.usuario.getUsername(), this.status, anexoDTOS);
    }

}

