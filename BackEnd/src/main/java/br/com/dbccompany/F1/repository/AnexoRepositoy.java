package br.com.dbccompany.F1.repository;


import br.com.dbccompany.F1.entity.Anexo;
import br.com.dbccompany.F1.entity.Chamado;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AnexoRepositoy extends CrudRepository<Anexo, Long> {

    Anexo findById (long id);

    List<Anexo> findAll();

    List<Anexo> findAllByChamado (Chamado chamado);
}
