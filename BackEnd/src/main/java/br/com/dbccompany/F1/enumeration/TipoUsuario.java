package br.com.dbccompany.F1.enumeration;

public enum TipoUsuario {
    COMUM, OPERADOR, INATIVO
}
