package br.com.dbccompany.F1.entity;

import br.com.dbccompany.F1.dto.MensagemDTO;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Mensagem.class)
public class Mensagem {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "mensagem_seq", sequenceName = "mensagem_seq")
    @GeneratedValue(generator = "mensagem_seq", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(length = 300, nullable = false)
    private String conteudo;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_chamado", nullable = false)
    private Chamado chamado;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_usuario", nullable = false)
    private Usuario usuario;

    @Column(name = "data_envio")
    private Date data = new Date(System.currentTimeMillis());

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConteudo() {
        return conteudo;
    }

    public Chamado getChamado() {
        return chamado;
    }

    public void setChamado(Chamado chamado) {
        this.chamado = chamado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public MensagemDTO geraDTO(){
        return new MensagemDTO(this.conteudo, this.data, this.usuario.getUsername(),this.usuario.getEmail());
    }
}
