package br.com.dbccompany.F1.verification;

public class MetodosDeVerificacao {
    public static boolean isEmailValid(String email){
        return email.matches("(\\w)+((\\.|(_)|(-)|(\\+))(\\w)+)*(@)(\\w)+((\\.)(\\w)+)+");
    }
}
