package br.com.dbccompany.F1.exceptions;

public class FileLimitExceededException extends RuntimeException {
    public FileLimitExceededException(String message){
        super(message);
    }

    public FileLimitExceededException(String message, Throwable cause){
        super(message, cause);
    }
}
