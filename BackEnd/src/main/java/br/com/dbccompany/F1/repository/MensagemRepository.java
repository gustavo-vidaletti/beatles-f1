package br.com.dbccompany.F1.repository;

import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Mensagem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MensagemRepository extends CrudRepository<Mensagem, Long> {

    Mensagem findById(long id);

    List<Mensagem> findAllByChamado(Chamado chamado);

}
