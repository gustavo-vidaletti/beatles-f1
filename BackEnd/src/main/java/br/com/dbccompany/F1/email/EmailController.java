package br.com.dbccompany.F1.email;

import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Mensagem;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class EmailController {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private UsuarioService usuarioService;

    private ArrayList<String> emailsDosOperadores(){
        ArrayList<String> emails = new ArrayList<>();
        for(Usuario user: usuarioService.buscarTodosPorTipo(TipoUsuario.OPERADOR)){
            emails.add(user.getEmail());
        }
        return emails;
    }

    public boolean chamadoCriado(Chamado chamado){
        ArrayList<String> todosEmails = emailsDosOperadores();
        todosEmails.add(chamado.getUsuario().getEmail());

        String[] emails = todosEmails.toArray(new String[todosEmails.size()]);

        String conteudo = MyConstants.conteudoCriacao(chamado);
        String assunto = MyConstants.assuntoEmail("Chamado criado", chamado);
        Thread thread = new Thread(new EnviarEmail(emails,assunto,conteudo,this.emailSender));
        thread.start();
        return true;
    }

    public boolean mensagemEnviada(Mensagem mensagem){
        String[] emails = null;
        if(mensagem.getUsuario().getTipoUsuario().equals(TipoUsuario.OPERADOR)){
            emails = MyConstants.emailsTo(mensagem.getChamado().getUsuario().getEmail());
        } else {
            emails = emailsDosOperadores().toArray(new String[emailsDosOperadores().size()]);
        }
        String assunto = MyConstants.assuntoEmail("Chamado respondido", mensagem.getChamado());
        String conteudo = MyConstants.conteudoResposta(mensagem);
        Thread thread = new Thread(new EnviarEmail(emails,assunto,conteudo,this.emailSender));
        thread.start();
        return true;
    }

    public boolean chamadoArquivado(Mensagem mensagem){
        String[] emails = emailsDosOperadores().toArray(new String[emailsDosOperadores().size()]);
        String assunto = MyConstants.assuntoEmail("Chamado arquivado", mensagem.getChamado());
        String conteudo = MyConstants.conteudoArquivado(mensagem);
        Thread thread = new Thread(new EnviarEmail(emails,assunto,conteudo,this.emailSender));
        thread.start();
        return true;
    }

    public boolean chamadoResolvido(Mensagem mensagem){
        String[] emails = MyConstants.emailsTo(mensagem.getChamado().getUsuario().getEmail());
        String assunto = MyConstants.assuntoEmail("Chamado resolvido", mensagem.getChamado());
        String conteudo = MyConstants.conteudoResolvido(mensagem);
        Thread thread = new Thread(new EnviarEmail(emails,assunto,conteudo,this.emailSender));
        thread.start();
        return true;
    }

}