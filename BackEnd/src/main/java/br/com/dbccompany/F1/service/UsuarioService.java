package br.com.dbccompany.F1.service;

import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.repository.UsuarioRepository;
import br.com.dbccompany.F1.verification.Criptografia;
import br.com.dbccompany.F1.verification.Permissoes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userDetailsService")
public class UsuarioService implements UserDetailsService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Transactional(rollbackFor = Exception.class)
    public Usuario salvar(Usuario usuario) throws Exception{
        usuario.setPassword(Criptografia.criptografarSenha(usuario.getPassword()));
        return usuarioRepository.save(usuario);
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuario editarPorId(long id, Usuario usuario) throws Exception{
        usuario.setId(id);
        return usuarioRepository.save(usuario);
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean deletarPorId(long id) throws Exception{
        boolean existia = (usuarioRepository.findById(id) != null);
        usuarioRepository.deleteById(id);
        return existia;
    }

    public Usuario buscarPorId(long id){
        return usuarioRepository.findById(id);
    }

    public Usuario buscarPorEmail(String email){
        return usuarioRepository.findByEmail(email);
    }

    public List<Usuario> listarTodos(){
        return usuarioRepository.findAll();
    }

    public List<Usuario> buscarTodosPorTipo(TipoUsuario tipoUsuario){
        return usuarioRepository.findALlByTipoUsuario(tipoUsuario);
    }

    public Usuario buscarPorUsername(String username){
        return usuarioRepository.findByUsername(username);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Usuario usuario = usuarioRepository.findByUsername(username);

        if (usuario == null) {
            throw new UsernameNotFoundException("login " + username + "não registrado");
        }

        List<GrantedAuthority> permissoes = Permissoes.definirRoles(usuario.getTipoUsuario());
        return new User(usuario.getUsername(), usuario.getPassword(), permissoes);

    }
}
