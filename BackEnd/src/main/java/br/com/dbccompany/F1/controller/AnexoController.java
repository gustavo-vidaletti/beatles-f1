package br.com.dbccompany.F1.controller;

import br.com.dbccompany.F1.entity.Anexo;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.exceptions.FileLimitExceededException;
import br.com.dbccompany.F1.service.AnexoService;
import br.com.dbccompany.F1.service.ChamadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@RestController
@RequestMapping("/f1/anexo")
public class AnexoController {

    @Autowired
    AnexoService anexoService;

    @Autowired
    ChamadoService chamadoService;

    @PostMapping("/{idChamado}")
    public Anexo adicionar(@RequestParam("file") MultipartFile file,
                           @PathVariable long idChamado) throws Exception {
        Chamado chamado = chamadoService.buscarPorId(idChamado);

        if(chamado.getAnexos().size() >= 3){
           throw new FileLimitExceededException("Limite de anexos por chamado excedido. (Máximo 3)");
        }

        String url = anexoService.store(file, idChamado);
        Anexo anexo = new Anexo();
        anexo.setUrl(url);
        anexo.setChamado(chamado);

        return anexoService.salvar(anexo);
    }

    @GetMapping(value = "/")
    public List<Anexo> listarTodos(){
        return anexoService.listarTodos();
    }

    @GetMapping(value = "/{id}")
    public Anexo buscar(@PathVariable long id){
        return anexoService.buscarPorId(id);
    }

    @GetMapping(value = "/chamado/{idChamado}")
    public List<Anexo> listarPorChamado(@PathVariable long idChamado){
        return anexoService.buscarTodosPorChamado(idChamado);
    }

    @PatchMapping(value = "/{id}")
    public Anexo editar(@PathVariable long id, @RequestParam("file") MultipartFile file ) throws Exception{
        return anexoService.editar(id, file);
    }


    @DeleteMapping(value = "/{id}")
    public boolean deletar(@PathVariable long id) throws Exception{
        return anexoService.deletar(id);
    }

    @GetMapping("/download/{id}")
    public StreamingResponseBody getSteamingFile(@PathVariable long id, HttpServletResponse response) throws IOException {
        Anexo anexo = anexoService.buscarPorId(id);
        InputStream inputStream = new FileInputStream(new File(anexo.getUrl()));
        return outputStream -> {
            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                System.out.println("Writing some bytes of file...");
                outputStream.write(data, 0, nRead);
            }
            inputStream.close();
            outputStream.close();
        };
    }

}
