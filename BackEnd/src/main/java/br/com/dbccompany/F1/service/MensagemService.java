package br.com.dbccompany.F1.service;

import br.com.dbccompany.F1.dto.MensagemDTO;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Mensagem;
import br.com.dbccompany.F1.repository.MensagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class MensagemService {

    @Autowired
    private MensagemRepository mensagemRepository;

    @Autowired
    private ChamadoService chamadoService;

    @Transactional(rollbackFor = Exception.class)
    public Mensagem salvar(Mensagem mensagem) throws Exception{
        return mensagemRepository.save(mensagem);
    }

    @Transactional(rollbackFor = Exception.class)
    public Mensagem editar(long id, Mensagem mensagem) throws Exception{
        mensagem.setId(id);
        return mensagemRepository.save(mensagem);
    }

    @Transactional(rollbackFor = Exception.class)
    public Mensagem atualizarConteudo(long id, String conteudo) throws Exception{
        Mensagem mensagem = mensagemRepository.findById(id);
        mensagem.setConteudo(conteudo);
        return mensagemRepository.save(mensagem);
    }

    public List<Mensagem> buscarTodasMensagens(){
        return (List<Mensagem>) mensagemRepository.findAll();
    }

    public Mensagem buscarPorId(long id){
        return mensagemRepository.findById(id);
    }

    public List<MensagemDTO> buscarPorChamado(long idChamado){
        Chamado chamado = chamadoService.buscarPorId(idChamado);
        List<MensagemDTO> msgsDTO = new ArrayList<>();
        for(Mensagem mensagem: mensagemRepository.findAllByChamado(chamado)){
            msgsDTO.add(mensagem.geraDTO());
        }
        return msgsDTO;
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean deletarPorId(long id) throws Exception{
        Mensagem mensagem = mensagemRepository.findById(id);
        boolean existia = (mensagem) != null;
        List<Mensagem> mensagens = mensagem.getChamado().getMensagens();
        mensagens.remove(mensagem);
        mensagem.getChamado().setMensagens(mensagens);
        chamadoService.salvar(mensagem.getChamado());
        mensagemRepository.deleteById(id);
        return existia;
    }

}
