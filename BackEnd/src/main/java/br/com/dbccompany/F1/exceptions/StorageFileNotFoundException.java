package br.com.dbccompany.F1.exceptions;

public class StorageFileNotFoundException extends br.com.dbccompany.F1.exceptions.StorageException {

    public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
