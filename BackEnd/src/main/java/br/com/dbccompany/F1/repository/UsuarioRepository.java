package br.com.dbccompany.F1.repository;

import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findById(long id);

    Usuario findByEmail(String email);

    Usuario findByUsername(String username);

    List<Usuario> findALlByTipoUsuario(TipoUsuario tipoUsuario);

    List<Usuario> findAll();

}
