package br.com.dbccompany.F1.dto;

import br.com.dbccompany.F1.enumeration.Prioridade;
import br.com.dbccompany.F1.enumeration.Status;

import java.util.Date;
import java.util.List;

public class ChamadoDTO {

    public ChamadoDTO(long id, String titulo, String descricao, Date dataCriacao, Date dataLimite, Prioridade prioridade, String username, Status status, List<AnexoDTO> anexos) {
        this.id = id;
        this.titulo = titulo;
        this.dataCriacao = dataCriacao;
        this.dataLimite = dataLimite;
        this.prioridade = prioridade;
        this.username = username;
        this.status = status;
        this.anexos = anexos;
        this.descricao = descricao;
    }

    long id;
    String titulo;
    String descricao;
    Date dataCriacao;
    Date dataLimite;
    Prioridade prioridade;
    String username;
    Status status;
    List<AnexoDTO> anexos;

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Date getDataLimite() {
        return dataLimite;
    }

    public void setDataLimite(Date dataLimite) {
        this.dataLimite = dataLimite;
    }

    public Prioridade getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(Prioridade prioridade) {
        this.prioridade = prioridade;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<AnexoDTO> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<AnexoDTO> anexos) {
        this.anexos = anexos;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
