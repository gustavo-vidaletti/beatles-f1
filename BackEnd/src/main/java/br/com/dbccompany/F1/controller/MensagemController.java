package br.com.dbccompany.F1.controller;

import br.com.dbccompany.F1.dto.MensagemDTO;
import br.com.dbccompany.F1.email.EmailController;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Mensagem;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.security.TokenAuthenticationService;
import br.com.dbccompany.F1.service.ChamadoService;
import br.com.dbccompany.F1.service.MensagemService;
import br.com.dbccompany.F1.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/f1/mensagem")
public class MensagemController {

    @Autowired
    private MensagemService mensagemService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ChamadoService chamadoService;

    @Autowired
    private EmailController emailController;

    @PreAuthorize("hasAnyRole('ROLE_OPERADOR', 'ROLE_COMUM')")
    @GetMapping(value = "/")
    public List<Mensagem> listarTodas(HttpServletRequest http) throws Exception{
        String username = TokenAuthenticationService.getUsername(http);
        Usuario usuario = usuarioService.buscarPorUsername(username);
        if(usuario.getTipoUsuario() != TipoUsuario.OPERADOR){
            throw new AccessDeniedException("Não tem permissao!");
        }
        return mensagemService.buscarTodasMensagens();
    }

    @PreAuthorize("hasAnyRole('ROLE_OPERADOR', 'ROLE_COMUM')")
    @GetMapping(value = "/chamado/{idChamado}")
    public List<MensagemDTO> listarPorChamado(@PathVariable long idChamado, HttpServletRequest http){
        String username = TokenAuthenticationService.getUsername(http);
        Usuario usuario = usuarioService.buscarPorUsername(username);
        if(usuario.getTipoUsuario() != TipoUsuario.OPERADOR &&
            usuario.getId() == chamadoService.buscarPorId(idChamado).getUsuario().getId()){
            return mensagemService.buscarPorChamado(idChamado);
        }
        return mensagemService.buscarPorChamado(idChamado);
    }

    @PreAuthorize("hasAnyRole('ROLE_OPERADOR', 'ROLE_COMUM')")
    @PostMapping(value = "/chamado/{idChamado}")
    public MensagemDTO adicionar(@RequestBody Mensagem mensagem,@PathVariable long idChamado, HttpServletRequest http) throws Exception{
        String username = TokenAuthenticationService.getUsername(http);
        Chamado chamado = chamadoService.buscarPorId(idChamado);
        Usuario usuario = usuarioService.buscarPorUsername(username);

        if(usuario.getId() != chamado.getUsuario().getId() && usuario.getTipoUsuario() != TipoUsuario.OPERADOR){
            throw new AccessDeniedException("Não tem permissao para isso!");
        }
        
        mensagem.setUsuario(usuario);
        mensagem.setChamado(chamado);
        mensagem =  mensagemService.salvar(mensagem);
        emailController.mensagemEnviada(mensagem);
        return mensagem.geraDTO();
    }

    @PreAuthorize("hasAnyRole('ROLE_OPERADOR', 'ROLE_COMUM')")
    @PatchMapping(value = "/{id}")
    public Mensagem editar(@PathVariable long id, @RequestBody String conteudo, HttpServletRequest http) throws Exception{
        String username = TokenAuthenticationService.getUsername(http);
        Usuario usuario = usuarioService.buscarPorUsername(username);
        Mensagem mensagem = mensagemService.buscarPorId(id);
        if(usuario.getId() != mensagem.getUsuario().getId()){
            throw new AccessDeniedException("Não tem permissão");
        }
        return mensagemService.atualizarConteudo(id, conteudo);
    }

    @PreAuthorize("hasAnyRole('ROLE_OPERADOR', 'ROLE_COMUM')")
    @DeleteMapping(value = "/{id}")
    public boolean deletar(@PathVariable long id, HttpServletRequest http) throws Exception{
        String username = TokenAuthenticationService.getUsername(http);
        Usuario usuario = usuarioService.buscarPorUsername(username);
        Mensagem mensagem = mensagemService.buscarPorId(id);
        if(usuario.getId() != mensagem.getUsuario().getId()){
            throw new AccessDeniedException("Não tem permissão para excluir essa mensagem!");
        }
        return mensagemService.deletarPorId(id);
    }

}
